const express = require('express');
const app = express();
// const bodyParser = require('body-parser');

const path = require('path');

const initAppRoutes = require('./server/startup/appRoutes');
const initAppTools = require('./server/startup/appTools')
const { botStart } = require('./server/utils/telegramBot')

const settings = require('./server/config');
const dbConnect = require('./server/utils/dbHelper');


app.use(express.json({ limit: '5mb' }));
app.use(express.urlencoded({ extended: true, limit: '5mb' }));


initAppTools(app);
initAppRoutes(app);


if (process.env.NODE_ENV === 'production') {

    app.use(express.static('client/build'));

    app.get('*', (req, res) => {

        res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'))

    })

}


const port = process.env.PORT || 5000;


process.on('unhandledRejection', err => console.error('UNHANDLED REJECTION:\n', err));
process.on('uncaughtException', err => console.error('UNCAUGHT EXCEPTION:\n', err));


const run = async () => {
    try {
        await dbConnect(settings.db);
        console.log('DB connected!!!');

        app.listen(port, async () => {
            await botStart()
            console.log(`Server listen on port ${port}`);
        })
    } catch (ex) {
        console.log('Connection failed!\n', ex);
    }
}

run();
