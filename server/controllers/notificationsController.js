const Notification = require('../models/notificationModel');
const handleAsyncErrors = require('../middlewares/handleAsyncErrors');
const { sendNotification } = require('../utils/telegramBot')



// GET LIST OF NOTIFICATIONS
module.exports.getListOfNotifications = handleAsyncErrors(
  async (req, res) => {

    const notificationsList = await Notification.find();

    res.status(200).json({ status: 'success', data: notificationsList });

  }
);


// GET NOTIFICATION BY ID
module.exports.getNotificationById = handleAsyncErrors(
  async (req, res) => {

    const notification = await Notification.findById(req.params.notificationId);

    res.status(200).json({ status: 'success', data: notification });

  }
);


// CREATE NEW NOTIFICATION
module.exports.createNotification = handleAsyncErrors(
  async (req, res) => {

    let { notification } = res.locals.notification ? res.locals : req.body;

    console.log('NOTIFICATION: ', notification);

    if (!notification) return;

    if (notification.category === 'callback') {

      notification.content = `<b><u>Заказ на звонок</u></b>\n\n<pre>${notification.senderName}</pre>\n<pre>${notification.senderPhone}</pre>`

    }

    await Promise.all([
      Notification.create(notification),
      await sendNotification(notification.content)
    ]);

    res.status(200).json({ status: 'success', data: res.locals.data ? res.locals.data : null })

  }
);


// REMOVE NOTIFICATION
module.exports.deleteNotification = handleAsyncErrors(
  async (req, res) => {

    const { notificationId } = req.params;

    const deletedNotification = await Notification.findByIdAndDelete(notificationId);

    res.status(201).json({ status: 'success', data: deletedNotification });

  });
  