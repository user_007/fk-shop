const Order = require('../models/orderModel');
const handleAsyncErrors = require('../middlewares/handleAsyncErrors')
const { sendNotification } = require('../utils/telegramBot')



// GET LIST OF ORDERS
module.exports.getListOfOrders = handleAsyncErrors(
  async (req, res) => {

    const ordersList = await Order.find();

    res.status(200).json({ status: 'success', data: ordersList });

  }
);


// GET ORDER BY ID
module.exports.getOrderById = handleAsyncErrors(
  async (req, res) => {

    const order = await Order.findById(req.params.orderId);

    res.status(200).json({ status: 'success', data: order });

  }
);


// CREATE NEW ORDER AND SEND IT TO SAVE IN CURRENT USER SESSION
module.exports.createOrder = handleAsyncErrors(
  async (req, res, next) => {

    // CREATE ORDER CONTENT WITH PRODUCT ID AND QUANTITY
    const content = [ { product: req.body.productId, quantity: req.body.quantity } ]
    const info = [{ state: 'новый' }]

    const newOrder = await Order.create({content: content, info: info});

    // SAVE NEW ORDER OBJECT IN OUR LOCALS
    res.locals.currentOrder = newOrder;

    // res.status(201).json({ status: 'success', data: newOrder });

    next();

  }
);


// UPDATE ORDER
module.exports.updateOrder = handleAsyncErrors(
  async (req, res) => {

    const { orderId } = req.params;

    const order = await Order.findOneAndUpdate(
      { _id: orderId },
      { content: req.body },
      { new: true }
    );    


    res.status(200).json({ status: 'success', data: order });

  });


// REMOVE ORDER
module.exports.deleteOrder = handleAsyncErrors(
  async (req, res) => {

    const { orderId } = req.params;

    const deletedOrder = await Order.findByIdAndDelete(orderId);

    res.status(201).json({ status: 'success', data: deletedOrder });

  });


  // ADD PRODUCT
module.exports.addProductToOrder = handleAsyncErrors(
  async (req, res, next) => {

    const { orderId } = req.params;    

    const order = await Order.addProductToOrder(orderId, { productId: req.body.productId, quantity: req.body.quantity })

    res.status(200).json({ status: 'success', data: order })

  }
)


module.exports.submitOrder = handleAsyncErrors(
  async (req, res, next) => {

    const { orderId } = req.params;
    const { order, orderData: { comment, ...owner } } = req.body


    const products = order.map( el => {

      return el.product.title + ' x ' + el.quantity

    } ).join(', ')
    let totalPrice = 0

    order.forEach( el => {

      // console.log(el)

      totalPrice += Number(el.quantity) * Number(el.product.price.value)

    } )

    const htmlMessage = `<b><u>НОВЫЙ ЗАКАЗ</u></b>\n\n<b>заказчик:</b>\n<pre>${owner.name}</pre>\n\n<b>телефон:</b>\n<pre>${owner.phone}</pre>\n\n<b>список товаров:</b>\n<pre><i>${products}</i>\n\n</pre><b>сумма заказа:</b> ${totalPrice} руб.
    `;

    const updatedOrder = await Order.findByIdAndUpdate(
      orderId, 
      { 
        content: { ...order },
        info: { $push: { state: 'отправлен' } },
        owner: owner,
        comment: comment
      }
    )

    res.locals.order = updatedOrder;
    res.locals.notification = {
      content: htmlMessage,
      senderPhone: owner.phone,
      senderName: owner.name,
      category: 'order'
    }

    // SEND TELEGRAM MESSAGE
    // await sendNotification(htmlMessage)

    next()

  }
)
  