const Session = require('../models/sessionModel');
const handleAsynxErrors = require('../middlewares/handleAsyncErrors');
const ErrorHandler = require('../utils/ErrorHandler')


// GET ALL SESSIONS
module.exports.getSessionsList = handleAsynxErrors(
  async (req, res, next) => {

    const listOfSessions = await Session.find();

    res.status(200).json({ status: 'success', data: listOfSessions })

  }
)


// GET SESSION BY ID
module.exports.getSessionById = handleAsynxErrors(
  async (req, res, next) => {

    // console.log('IP: ', req.ip);
    // console.log('IPs: ', req.ips);

    const session = await Session.findByIdAndUpdate(req.params.sessionId, { $push: {visits: Date.now()} });

    res.status(200).json({ status: 'success', data: session })

  }
)


// ADD NEW SESSION
module.exports.createSession = handleAsynxErrors(
  async (req, res, next) => {

    const newSession = await Session.create(req.body);

    if (!newSession) return next(new ErrorHandler('Не удалось создать сессию', 400))

    res.status(200).json({ status: 'success', data: newSession })

  }
)


// UPDATE SESSION
module.exports.updateSession = handleAsynxErrors(
async (req, res, next) => {

    const { sessionId } = req.params;

    // console.log('UPDATE SESSION: ', res.locals.order);

    const updatedSession = res.locals.order ? 
      await Session.findOneAndUpdate({ currentOrder: res.locals.order._id }, { currentOrder: null }, { new: true } ) 
      :
      await Session.findByIdAndUpdate(sessionId, req.body, { new: true });

    res.locals.data = { ...updatedSession }

    next()

  }
)


// DELETE SESSION
module.exports.deleteSession = handleAsynxErrors(
  async (req, res, next) => {

    const deletedSession = await Session.findByIdAndDelete(req.params.sessionId);

    res.status(200).json({ status: 'success', data: deletedSession })

  }
)


// ADD ORDER TO SESSION
module.exports.addCurrentOrder = handleAsynxErrors(
  async (req, res, next) => {

    // CHECK IF WE HAVE CURRENT ORDER OBJECT IN OUR LOCALS
    if (!res.locals.currentOrder) throw new ErrorHandler('Заказ не найден!', 400)


    //UPDATE SESSION
    const updatedSession = await Session.findByIdAndUpdate(req.params.sessionId, { currentOrder: res.locals.currentOrder._id });

    // updatedSession.currentOrder = res.locals.currentOrder;

    // RETURN CURRENT ORDER
    res.status(200).json({ status: 'success', data: res.locals.currentOrder })

  }
)


// SUBMIT
module.exports.submitCurrentOrder = handleAsynxErrors(
  async (req, res) => {

    //UPDATE SESSION
    const updatedSession = await Session.findOneAndUpdate({ currentOrder: res.locals.order._id }, { currentOrder: null });

    // updatedSession.currentOrder = res.locals.currentOrder;

    // RETURN CURRENT ORDER
    res.status(200).json({ status: 'success', data: updatedSession })

  }
)
