const bcrypt = require('bcrypt');
const jwt = reuire('jsonwebtoken');
const _ = require('lodash')

const User = require('../models/userModel');
const ErrorHandler = require('../utils/ErrorHandler')
const handleAsyncErrors = require('../middlewares/handleAsyncErrors')
const { jwtSecretKey } = require('../config')


module.exports.getUserData = handleAsyncErrors(

  async (req, res, next) => {

    const userData = await User.findById(req.params.userId)

    res.status(200).json({ status: 'success', data: userData })

  }

)


module.exports.registerNewUser = handleAsyncErrors(

  async (req, res, next) => {

    const { password, ...otherUserData } = req.body;

    const numOfSameEmailUsers = await User.find({ email: otherUserData.email }).estimatedDocumentCount()

    if (numOfSameEmailUsers > 0) throw new ErrorHandler(`Пользователь с электронной почтой ${otherUserData.email} уже существует`, 400)

    const hashedPassword = await bcrypt.hash(password, 12)

    await User.create({ ...otherUserData, password: hashedPassword })

    res.status(200).json({ succss: true, data: otherUserData })

  }

)


module.exports.loginUser = handleAsyncErrors(

  async (req, res, next) => {

    const { email, password } = req.body;

    const user = await User.findOne({ email: email });

    const match = await bcrypt.compare(password, user.password)

    if (!match) throw new Error('неверный логин или пароль')

    const userData = _.omit(user, ['password'])

    const token = jwt.sign(userData, jwtSecretKey)

    // return JWT token with user name and roles
    res.status(200).json({ success: true, data: token })

  }

)


module.exports.ensureHasRights = (rights) => handleAsyncErrors(

  async (req, res, next) => {

    const rightsList = typeof rights === "string" ? [rights] : [ ...rights ]

    const token = req.headers['X-AUTH-TOKEN']

    const decoded = jwt.verify(token, jwtSecretKey)

    const user = await User.findById(decoded._id)

    // if (_.intersection(user.roles, rightsList).length === rightsList.length)
    if (user.roles.some( role => rightsList.includes(role))) return next();

    throw new ErrorHandler('У Вас недостаточно прав', 403)

  }

)


module.exports.removeUser = handleAsyncErrors(

  async (req, res, next) => {

    const numOfAdminUsers = await User.find({ roles: 'admin' }).estimatedDocumentCount()

    if (numOfAdminUsers === 1) throw new ErrorHandler('Нужно назначить хотя бы еще 1 админа, перед удалением', 400)

    await User.findByIdAndRemove(req.params.userId)

    res.status(200).json({ success: true })

  }

)

