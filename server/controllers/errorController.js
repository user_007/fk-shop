module.exports = (err, req, res, next) => {

  const {

    message = '',
    statusCode = 500,
    status = 'error' 
    
  } = err;

  console.error('Error handler:\n', err);


  return res.status(statusCode).json({ status, message });

}