const Notification = require('../models/notificationModel')
const Product = require('../models/productModel')
const Order = require('../models/orderModel')
const handleAsyncErrors = require('../middlewares/handleAsyncErrors')


exports.getAdminMainData = handleAsyncErrors(
  async (req, res, next) => {


    const data = [
      Product.find().estimatedDocumentCount(),
      Notification.find().estimatedDocumentCount(),
      Order.find().estimatedDocumentCount(),
      Product.find()
      .limit(20)
      .select('_id title brand category price image')
      .populate({ path: 'brand', select: 'name' })
      .populate({ path: 'category', select: 'name' })
    ]

    const [numProducts, numNotifications, numOrders, products] = await Promise.all(data)

    res.status(200).json({ status: 'success', data: {

      stats: { products: numProducts, notifications: numNotifications, orders: numOrders },
      products: products

    } })

  }
)

