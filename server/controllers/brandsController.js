const Brand = require('../models/brandModel');
const handleAsyncErrors = require('../middlewares/handleAsyncErrors')



// GET LIST OF BRANDS
module.exports.getListOfBrands = handleAsyncErrors(
  async (req, res) => {

    const brandsList = await Brand.find();

    res.status(200).json({ status: 'success', data: brandsList });

  }
);


// GET BRAND BY ID
module.exports.getBrandById = handleAsyncErrors(
  async (req, res) => {

    const brand = await Brand.findById(req.params.brandId);

    res.status(200).json({ status: 'success', data: brand });

  }
);


// CREATE NEW BRAND
module.exports.createBrand = handleAsyncErrors(
  async (req, res) => {

    const newBrand = await Brand.create(req.body);

    res.status(201).json({ status: 'success', data: newBrand });

  }
);


// UPDATE BRAND
module.exports.updateBrand = handleAsyncErrors(
  async (req, res) => {

    const { brandId } = req.params;

    const updatedBrand = await Brand.findByIdAndUpdate(brandId, req.body)

    res.status(201).json({ status: 'success', data: updatedBrand });

  });


// REMOVE BRAND
module.exports.deleteBrand = handleAsyncErrors(
  async (req, res) => {

    const { brandId } = req.params;

    const deletedBrand = await Brand.findByIdAndDelete(brandId);

    res.status(201).json({ status: 'success', data: deletedBrand });

  });
  