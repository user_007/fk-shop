const Product = require('../models/productModel');
const handleAsyncErrors = require('../middlewares/handleAsyncErrors')


// GET LIST OF ALL PRODUCTS
module.exports.getListOfProducts = handleAsyncErrors(
  async (req, res, next) => {

    const { brands, categories } = req.query;

    const brandsList = brands && brands.length ? brands.split(',') : []
    const categoriesList = categories && categories.length ? categories.split(',') : []


    const products = await Product.getFilteredListOfProducts({
      brands: brandsList, categories: categoriesList
    })


    res.status(200).json({ status: 'success', data: products })

  }
)


// GET PRODUCT DATA BY SLUG
module.exports.getProductData = handleAsyncErrors(
  async (req, res, next) => {

    const product = await Product.findOne({ slug: req.params.productSlug });

    res.status(200).json({ status: 'success', data: product })

  }
)


// ADD NEW PRODUCT
module.exports.addNewProduct = handleAsyncErrors(
  async (req, res, next) => {

    const product = await Product.create(req.body);

    res.status(200).json({ status: 'success', data: product })

  }
)


// UPDATE PRODUCT
module.exports.updateProduct = handleAsyncErrors(
  async (req, res, next) => {

    const { productId } = req.params;

    const updatedProduct = await Product.findByIdAndUpdate(productId, req.body);

    res.status(200).json({ status: 'success', data: updatedProduct });

  }
)


// DELETE PRODUCT
module.exports.deleteProduct = handleAsyncErrors(
  async (req, res, next) => {

    const { productId } = req.params;

    const deletedProduct = await Product.findByIdAndDelete(productId);

    res.status(200).json({ status: 'success', data: deletedProduct });

  }
)


// GET DATA FOR MAIN PAGE
module.exports.getBrandSortedProducts = handleAsyncErrors(
  async (req, res, next) => {

    const data = await Product.getBrandSortedProducts();

    res.status(200).json({ status: 'success', data: data })

  }
)
