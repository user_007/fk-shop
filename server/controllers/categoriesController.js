const Category = require('../models/categoryModel');
const handleAsynxErrors = require('../middlewares/handleAsyncErrors');
const ErrorHandler = require('../utils/ErrorHandler')


// GET ALL CATEGORIES
module.exports.getCategoriesList = handleAsynxErrors(
  async (req, res, next) => {

    const listOfCategories = await Category.find();

    res.status(200).json({ status: 'success', data: listOfCategories })

  }
)


// GET CATEGORY BY ID
module.exports.getCategoryById = handleAsynxErrors(
  async (req, res, next) => {

    const category = await Category.findById(req.params.categoryId);

    res.status(200).json({ status: 'success', data: category })

  }
)


// ADD NEW CATEGORY
module.exports.createCategory = handleAsynxErrors(
  async (req, res, next) => {

    const newCategory = await Category.create(req.body);

    if (!newCategory) return next(new ErrorHandler('Не удалось создать категорию', 400))

    res.status(200).json({ status: 'success', data: newCategory })

  }
)


// UPDATE CATEGORY
module.exports.updateCategory = handleAsynxErrors(
  async (req, res, next) => {

    const { categoryId } = req.params;

    const updatedCategory = await Category.findByIdAndUpdate(categoryId, req.body);

    res.status(200).json({ status: 'success', data: updatedCategory })

  }
)


// DELETE CATEGORY
module.exports.deleteCategory = handleAsynxErrors(
  async (req, res, next) => {

    const deletedCategory = await Category.findByIdAndDelete(req.params.categoryId);

    res.status(200).json({ status: 'success', data: deletedCategory })

  }
)
