const router = require('express').Router();

const {
  getListOfBrands, getBrandById, updateBrand, createBrand, deleteBrand
} = require('../controllers/brandsController');


router.get('/', getListOfBrands);

router.post('/', createBrand);

router.get('/:brandId', getBrandById);

router.delete('/:brandId', deleteBrand);

router.patch('/:brandId', updateBrand);


module.exports = router;