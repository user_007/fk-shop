const router = require('express').Router();

const {
  getListOfOrders, getOrderById, deleteOrder, createOrder, updateOrder, addProductToOrder, submitOrder
} = require('../controllers/ordersController');
const { updateSession } = require('../controllers/sessionsController')
const { createNotification } = require('../controllers/notificationsController')


router.get('/', getListOfOrders)

router.post('/', createOrder);

router.get('/:orderId', getOrderById)

router.post('/:orderId', updateOrder)

router.post('/:orderId/submit', submitOrder, updateSession, createNotification)

router.delete('/:orderId', deleteOrder)

router.post('/:orderId/add-product', addProductToOrder);


module.exports = router;
