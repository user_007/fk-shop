const router = require('express').Router();

const {
  getListOfNotifications, createNotification, deleteNotification, getNotificationById
} = require('../controllers/notificationsController');


router.get('/', getListOfNotifications)

router.post('/', createNotification);

router.get('/:notificationId', getNotificationById)

router.delete('/:notificationId', deleteNotification)


module.exports = router;