const router = require('express').Router();

const {
  getSessionById, getSessionsList, updateSession, deleteSession, createSession, addCurrentOrder
} = require('../controllers/sessionsController');


const { createOrder } = require('../controllers/ordersController');


router.get('/', getSessionsList)

router.post('/', createSession);

router.post('/:sessionId/add-order', createOrder, addCurrentOrder)

router.get('/:sessionId', getSessionById)

router.post('/:sessionId', updateSession)

router.delete('/:sessionId', deleteSession)


module.exports = router;
