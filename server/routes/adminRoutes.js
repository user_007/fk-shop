const router = require('express').Router();

const { getAdminMainData } = require('../controllers/adminController')


router.get('/', getAdminMainData)


module.exports = router;
