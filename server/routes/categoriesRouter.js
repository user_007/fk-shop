const router = require('express').Router();

const {
  createCategory, updateCategory, deleteCategory, getCategoriesList, getCategoryById
} = require('../controllers/categoriesController');


router.get('/', getCategoriesList)

router.post('/', createCategory);

router.get('/:categoryId', getCategoryById)

router.delete('/:categoryId', deleteCategory)

router.patch('/:categoryId', updateCategory)


module.exports = router;
