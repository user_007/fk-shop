const router = require('express').Router();

const { 

  addNewProduct, deleteProduct, updateProduct, getListOfProducts, getProductData, getBrandSortedProducts

} = require('../controllers/productController');



router.get('/', getListOfProducts);

router.get('/brand-sorted', getBrandSortedProducts);

router.post('/', addNewProduct);

router.get('/:productSlug', getProductData);

router.delete('/:productId', deleteProduct);

router.patch('/:productId', updateProduct);
  

module.exports = router;
