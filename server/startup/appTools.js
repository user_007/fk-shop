const xss = require('xss-clean');
const helmet = require('helmet');
const cors = require('cors');
const mongoSanitize = require('express-mongo-sanitize');
const rateLimit = require('express-rate-limit');


module.exports = app => {

  app.set('trust proxy', true)

  // 1) GLOBAL MIDDLEWARES
  // Implement CORS
  app.use(cors());
  // Access-Control-Allow-Origin *

  app.options('*', cors());

  // Set security HTTP headers
  app.use(helmet());

  // Limit requests from same API
  const limiter = rateLimit({
    max: 100,
    windowMs: 60 * 60 * 1000,
    message: 'Превышен лимит запросов с Вашего IP адреса!'
  });
  app.use('/api', limiter);

  // Data sanitization against NoSQL query injection
  app.use(mongoSanitize());

  // Data sanitization against XSS
  app.use(xss());

}