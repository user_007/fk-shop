const productsRouter = require('../routes/productsRouter');
const categoriesRouter = require('../routes/categoriesRouter');
const brandsRouter = require('../routes/brandsRouter');
const notificationsRouter = require('../routes/notificationsRouter');
const ordersRouter = require('../routes/ordersRouter');
const sessionsRouter = require('../routes/sessionsRouter');
const adminRouter = require('../routes/adminRoutes');

const ErrorHandler = require('../utils/ErrorHandler');
const errorResponse = require('../controllers/errorController')


module.exports = app => {

  app.use('/api/v1/products', productsRouter);
  app.use('/api/v1/categories', categoriesRouter);
  app.use('/api/v1/brands', brandsRouter);
  app.use('/api/v1/notifications', notificationsRouter);
  app.use('/api/v1/orders', ordersRouter);
  app.use('/api/v1/sessions', sessionsRouter);
  app.use('/api/v1/admin', adminRouter);

  app.all('/api/*', (req, res, next) => {
    next(new ErrorHandler(`По данному пути (${req.originalUrl}) ничего не найдено!`, 404));
  });

  app.use(errorResponse);

}
