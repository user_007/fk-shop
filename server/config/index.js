const { 

  DB_URL, 
  BOT_TOKEN, 
  MANAGER_CHAT_ID, 
  JWT_SECRET_KEY 

} = process.env.NODE_ENV === 'production' ? process.env : require('../../local-config');


module.exports = {

  db: DB_URL,
  botToken: BOT_TOKEN,
  chatId: MANAGER_CHAT_ID,
  jwtSecretKey: JWT_SECRET_KEY

}
