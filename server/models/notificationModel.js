const mongoose = require('mongoose');


const notificationSchema = new mongoose.Schema({

  content: { 
    type: String, 
    require: [ true, 'Необходимо написать сообщение' ],
    minlength: [ 5, 'Название должно быть от 3 символов' ]
  },
  senderName: {
    type: String,
    require: [ true, 'Нужно указать имя отправителя' ],
    minlength: [ 3, 'Имя должно содержать не менее 5 букв' ]
  },
  senderPhone: {
    type: String,
    require: [ true, 'Необходимо указать телефон для связи' ]
  },
  category: {
    type: String,
    enum: ['callback', 'order'],
    default: 'callback'
  }

});


module.exports = mongoose.model('notifications', notificationSchema);
