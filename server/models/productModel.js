const mongoose = require('mongoose');
const slugify = require('limax')


const productSchema = new mongoose.Schema({

  title: {
    type: String,
    minlength: [ 3, 'Название должно состоять хотя бы из 3 символов' ],
    unique: [ true, 'Такое название уже существует!' ]
  },
  slug: { type: String, unique: [ true, 'ЧПУ должен быть уникальным' ] },
  image: {
    type: String,
    //add default image
    default: 'https://res.cloudinary.com/dshj8uydq/image/upload/v1586843052/default-product.jpg'
  },
  price: {
    value: { type: Number, default: 0 },
    description: String
  },
  purpose: {
    type: String,
    require: [ true, 'Необходимо заполнить для товара "применение"' ]
  },
  description: {
    type: String,
    require: [ true, 'Необходимо заполнить описание' ]
  },
  brand: {
    type: mongoose.Schema.ObjectId,
    ref: 'Brand'
  },
  category: {
    type: mongoose.Schema.ObjectId,
    ref: 'Category'
  },
  properties: [
    {
      title: String,
      content: String
    }
  ]
})


productSchema.pre('save', function(next) {

  this.slug = slugify(this.title, { lang: 'ru' })

  next();

})

// productSchema.pre(/^find/, function(next) {

  // console.log(this._conditions);

  // this.populate({ path: 'category', select: '-__v' })
  // this.populate({ path: 'brand', select: '-__v' })
  

  // next();

// })


productSchema.statics.getBrandSortedProducts = async function() {

  return await this.aggregate([    
    {
      $group: {
        _id: '$brand',
        products: { $push: "$$ROOT" }
      } 
    },
    {
      $project: {
        "products.brand": 0
      }
    },
    {
      $lookup: {
        from: "brands",
        localField: "_id",
        foreignField: "_id",
        as: "brand"
      }
    },
    {
      $unwind: "$brand"
    },
    {
      $addFields: { brand_desc: "$brand.description", brand_name: "$brand.name" }
    },
    {
      $project: {
        "brand": 0
      }
    }
  ])

}


productSchema.statics.getFilteredListOfProducts = async function({ brands = [], categories = [] }) {

  let matchPipeline = []

  if (brands.length) matchPipeline.push({'$match': { 'brand.name': { $in: brands } }})
  if (categories.length) matchPipeline.push({'$match': { 'category.name': { $in: categories } }})


  return await this.aggregate([
    {
      $lookup: {
        from: "brands",
        localField: "brand",
        foreignField: "_id",
        as: "brandData"
      }
    },
    {
      $unwind: "$brandData"
    },
    {
      $lookup: {
        from: "categories",
        localField: "category",
        foreignField: "_id",
        as: "categoryData"
      }
    },
    {
      $unwind: "$categoryData"
    },
    {
      $project: {
        _id: 1,
        "brand": "$brandData",
        "category": "$categoryData",
        title: 1,
        price: 1,
        slug: 1,
        image: 1
      }
    },  
    ...matchPipeline
  ])

}


// title: 'Клей для газобетона',
// image: 'https://res.cloudinary.com/dshj8uydq/image/upload/v1560424567/k0j4tldio6kddhtv02jg.png',
// price: { value: '215', description: '' },
// brand: 'Felker',
// category: 'Клей',
// purpose: 'Предназначен для приклеивания блоков пено- и газобетона',
// description: 'Кладочно-монтажный клей для наружных и внутренних работ предназначен для кладки стен и перегородок из блоков и плит на основе ячеистых бетонов (пенобетона и газобетона), газосиликата, силикатных блоков и плит. Возможно использование клея при заделке сколов и выбоин плит из газо- и пенобетона до 10 см.'

module.exports = mongoose.model('products', productSchema)
