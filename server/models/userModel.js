const mongoose = require('mongoose');


const userSchema = new mongoose.Schema({

  email: String,
  password: String,
  name: String,
  registerDate: {
    type: Date,
    default: Date.now
  },
  roles: [{
    type: String,
    default: 'пользователь',
    enum: ['гость', 'админ', 'суперадмин', 'пользователь', 'модератор']
  }]

})

module.exports = mongoose.model('User', userSchema)

