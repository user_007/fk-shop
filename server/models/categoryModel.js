const mongoose = require('mongoose');


const categorySchema = new mongoose.Schema({

  name: { 
    type: String, 
    require: [ true, 'Необходимо указать название категории' ],
    minlength: [ 3, 'Название должно быть от 3 символов' ],
    unique: [ true, 'Такое название уже существует' ]
  },
  image: String

});


module.exports = mongoose.model('Category', categorySchema)
