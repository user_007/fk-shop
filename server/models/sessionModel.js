const mongoose = require('mongoose');


const sessionSchema = new mongoose.Schema({

  currentOrder: {
    type: mongoose.Schema.ObjectId,
    ref: 'Order'
  },
  visits: [
    { 
      type: Date, 
      default: Date.now
    }
  ],
  name: String,
  phone: String

});

sessionSchema.pre('save', function(next) {

  this.visits.push(Date.now())

  next();

})


sessionSchema.pre(/^find/, function(next) {

  this.populate('currentOrder')  

  next();

})


sessionSchema.pre(/^find/, function(next) {

  // this.populate('currentOrder.content.product').select('title image brand price category _id')

  next();

})

sessionSchema.post(/Remove|Delete$/, function() {

  // do we need to delete all Orders that were belonged to session???

})


module.exports = mongoose.model('Session', sessionSchema)
