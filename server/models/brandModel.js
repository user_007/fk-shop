const mongoose = require('mongoose');


const brandSchema = new mongoose.Schema({

  name: { 
    type: String, 
    require: [ true, 'Необходимо указать название брэнда' ],
    minlength: [ 3, 'Название должно быть от 3 символов' ],
    unique: [ true, 'Такое название уже существует' ]
  },
  image: String,
  description: String

});


module.exports = mongoose.model('Brand', brandSchema)
