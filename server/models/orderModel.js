const mongoose = require('mongoose');
const ErrorHandler = require('../utils/ErrorHandler')
const Product = require('./productModel')


const orderSchema = new mongoose.Schema({

  content: [
    {
      product: {
        type: mongoose.Schema.ObjectId,
        ref: 'products'
      },
      quantity: {
        type: Number,
        min: [ 1, 'количество товара не может быть меньше 1' ],
        max: [ 99, 'количество товара не может быть больше 99' ]
      }
    }
  ],
  createdDate: {
    type: Date,
    default: Date.now
  },
  info: [
    {
      state: {
        type: String,
        enum: ['новый', 'отправлен', 'доставлен', 'отменен'],
        default: 'новый'
      },
      date: {
        type: Date,
        default: Date.now
      }
    }
  ],
  owner: {
    name: String,
    phone: String
  },
  comment: String

});


orderSchema.statics.addProductToOrder = async function(orderId, { productId, quantity = 1 }) {

  let order = await this.findById(orderId)

  const prodIndex = order.content.findIndex(el => {

    // console.log('order content element: ', el);

    const cond = ''+el.product._id == ''+productId

    // console.log('COND: ', cond);

    return cond;

  });


  if (prodIndex < 0) {

    if (quantity < 1) throw new ErrorHandler('Указано неверное количество товара', 400) 

    order.content.push({ product: productId, quantity: quantity })

  } else {

    const totalIsGtZero = (order.content[prodIndex].quantity - quantity) > 0

    totalIsGtZero ? 
      order.content[prodIndex].quantity += quantity : 
      order.content.filter( (el, index) => index != prodIndex )

  }

  await order.save()
  order.populate('content.product')

  return order;

}


orderSchema.statics.changeOrderContent = async function(orderId, newContent) {

  const order = await this.aggregate([

    { $match: { _id: orderId } },
    { $set: { content: { $each: newContent } } },

  ])

  return order

}


orderSchema.pre('find', function(next) {

  this.populate({ path: 'content.product', select: '-purpose -properties -description' });

  next()

})


orderSchema.pre(/^find(One|By)/, function(next) {

  this.populate('content.product')

  next()

})


orderSchema.post('save', async function() {

  const lastIndex = this.content.length - 1;

  // TODO: FIND ANOTHER WAY TO POPULATE LAST PRODUCT IN ORDER CONTENT
  const lastOrder = await Product
    .findById(this.content[lastIndex].product)
    .populate('brand category');

  this.content[lastIndex].product = lastOrder;

  // this.populate('content.product')

})


// 

module.exports = mongoose.model('Order', orderSchema)
