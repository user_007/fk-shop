class ErrorHandler extends Error {

  constructor(message, statusCode) {

    super(message);

    this.statusCode = statusCode;
    this.status = `${statusCode}`.startsWith('4') ? 'fail' : 'error'


    Error.captureStackTrace(this, this.constructor);

    console.error('ERROR!!!!:\n', this)

  }

}


module.exports = ErrorHandler;
