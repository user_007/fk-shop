const TelegramBot  = require('node-telegram-bot-api')

const { botToken, chatId } = require('../config')

const bot = new TelegramBot(botToken, { polling: true })


module.exports.sendNotification = async message => {

  try {    

    return await bot.sendMessage(chatId, message, { parse_mode: 'HTML' })
    
  } catch (err) {

    console.error('TELEGRAM BOT ERROR: ', err)
    return err.message
    
  }

}


exports.botStart = async () => {

  if (!bot.isPolling) await bot.startPolling()

}
