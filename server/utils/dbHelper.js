const mongoose = require('mongoose');

mongoose.Promise = global.Promise;


let dbConnection = {

  instance: null

}


module.exports = async url => {

  if (!dbConnection.instance)  dbConnection.instance = await mongoose.connect(
    
    url, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true, useFindAndModify: false }
  
  )

} 
