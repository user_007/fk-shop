import React, { Component } from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'
import { FaSort } from 'react-icons/fa'

import ProductCard from '../../components/product-card'
import PageTopline from '../../components/page-topline'
import LoadingScreen from '../../components/loading-screen'
import ProductFilters from '../../components/product-filters'

import { addProductToCart } from '../../actions/cartActions'
import { loadPageData } from '../../actions/dataActions'

import './styles.css'



class ProductsCatalog extends Component {

  state = {

    loading: true,
    processingRequest: false,
    applyFiltersPending: null,
    filters: [],
    sort: 'price'

  }


  async componentDidMount() {

    const { location: { state: dataToLoad }, history, loadPageData } = this.props

    const brandsFilters = dataToLoad && dataToLoad.brands ? dataToLoad.brands : []
    const categoriesFilters = dataToLoad && dataToLoad.categories ? dataToLoad.categories : []

    await loadPageData(history, dataToLoad)

    try {

      this.setState({
        loading: false,
        filters: [...brandsFilters, ...categoriesFilters]
      })

    } catch (err) {

      console.error(err)

    }

  }

  async componentDidUpdate(prevProps) {

    const { history, location: { state: dataToLoad }, loadPageData } = this.props

    // eslint-disable-next-line
    if (dataToLoad != prevProps.location.state) {

      const brandsFilters = dataToLoad && dataToLoad.brands ? dataToLoad.brands : []
      const categoriesFilters = dataToLoad && dataToLoad.categories ? dataToLoad.categories : []

      this.setState({
        filters: [ ...brandsFilters, ...categoriesFilters ]
      })

      await loadPageData(history, dataToLoad)

    }

  }


  requestToAddProductToCart = async productId => {

    this.setState({ processingRequest: true })

    const { addProductToCart, cart: { order } } = this.props;

    const orderId = order ? order._id : null;

    await addProductToCart(orderId, { productId, quantity: 1 })

    this.setState({ processingRequest: false })

  }


  handleFilterChange = filterName => {

    const { applyFiltersPending, filters } = this.state;

    clearTimeout(applyFiltersPending);

    const filtersList = filters.includes(filterName) ? 
      filters.filter(el => el !== filterName) : [ ...filters, filterName ]

    this.setState({

        filters: filtersList,
        applyFiltersPending: setTimeout(this.applyFilters, 1000)

    })

  }


  applyFilters = async () => {

    const { filters } = this.state;
    let brands = [], categories = [];

    filters.forEach(filter => {

      ['rockwool', 'felker'].includes(filter.toLowerCase()) ? brands.push(filter) : categories.push(filter)


    })

    await this.props.loadPageData(this.props.history, { brands: brands, categories: categories })

  }


  handleSortMethodChange = e => this.setState({ sort: e.target.value })



  render() {

    if (this.state.loading || this.props.page.loading) return <div className="content"><LoadingScreen /></div>

    const { processingRequest, filters, sort } = this.state;
    const { cart: { order }, page: { categories, brands, content: products } } = this.props


    const allFilters = [
      {
        title: 'Брэнды материалов',
        items: brands.map(brand => ({ name: brand.name, active: filters.includes(brand.name) }))
      },
      {
        title: 'Категории материалов',
        items: categories.map(category => ({ name: category.name, active: filters.includes(category.name) }))
      }
    ]

    const sortField = sort === 'price' ? sort + '.value' : sort + '.name';

    const productCards = _.sortBy(products, sortField)
    .map(product => {

      const isAddedToCart = order && order.content.some( el => el.product._id === product._id );
      // const isAddedToCart = false;

      return <ProductCard 
                key={product._id}
                product={ product } isAddedToCart={ isAddedToCart }
                handleOnclick={ this.requestToAddProductToCart } 
             />

    });

    const sortTypes = <select 
      className="form-control label-sm ml-2" 
      name="sort_method" id="sort_method" value={ sort }
      onChange={ this.handleSortMethodChange }
    >
      <option value="price">По цене за упаковку</option>
      <option value="category">По типу материала</option>
      <option value="brand">По брэндам</option>
    </select>


    return (
      <div className="content">
        
        { processingRequest && <LoadingScreen /> }
        <div className="container">

          <PageTopline pageData={ this.props.page } title="Каталог продуктов" >
            <div className="product-catalog__tools">
              {/* {brandsSort} */}
              Упорядочить: {sortTypes}
            </div>
          </PageTopline>
          <div className="row">

            <div className="col-md-3">

              <ProductFilters onFilterChange={ this.handleFilterChange } filters={ allFilters } />

            </div>
            <div className="col-md-9">
              {
                productCards && productCards.length ?
                <div className="row">{productCards}</div>
                :
                <div className="no-content">Товаров не найдено</div>
              }
              
            </div>

          </div>

        </div>

      </div>
    )
  }
}

const mapStateToProps = ({ page, cart }) => ({ page, cart })

const mapDispatchToProps = {

  addProductToCart, loadPageData

}


export default connect(mapStateToProps, mapDispatchToProps)(ProductsCatalog);
