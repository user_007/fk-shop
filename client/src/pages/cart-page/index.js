import React, { Component } from 'react'
import { connect } from 'react-redux'
// import PropTypes from 'prop-types'
// eslint-disable-next-line
import { Link } from 'react-router-dom'
import { Modal, Alert, Form, Button, OverlayTrigger, Tooltip } from 'react-bootstrap'
import { FaTrash, FaRubleSign, FaReply, FaEraser, FaPaperPlane, FaSpinner } from 'react-icons/fa'
import ProductQuantity from '../../components/product-quantity'
import LoadingScreen from '../../components/loading-screen'

import { updateCurrentOrder, submitOrder } from '../../actions/cartActions'

import './style.css'



class CartPage extends Component {

  state = {
    products: [],
    delivery: [
      { type: 'самовывоз', price: 0, chosen: true },
      { type: 'по Севастополю', price: 500, chosen: false },
      { type: 'в другой город', price: 1500, chosen: false },
    ],
    loading: true,
    updateTimer: null,
    showModal: false,
    orderForm: {
      name: '',
      phone: '',
      comment: ''
    }
  }



  async componentDidMount() {

    // console.log('CART PAGE MOUNTED PROPS: ', this.props);

    const { order } = this.props.cart;

    const products = order ? order.content : [];

    this.setState({ products: [...products], loading: false })

  }


  static getDerivedStateFromProps(props, state) {

    if (props.dataFetched && props.cart.order && props.cart.order.content.length > 0 && state.products.length === 0) {

      return {
        products: [...props.cart.order.content],
        loading: false
      };
    }

    if (!props.cart.order) {

      return {
        products: []
      }

    }


    if (!state.loading && !props.dataFetched) return { loading: true }

    return null;
  }



  handleFormFieldChange = e => {

    const { id, value } = e.target

    this.setState(state => ({ orderForm: { ...state.orderForm, [id]: value } }))

  }


  handleOrderSubmit = async e => {

    e.preventDefault();

    if (this.state.loading) return;

    this.setState({ loading: true })

    const { products, orderForm } = this.state
    const { submitOrder, cart: { order } } = this.props

    await submitOrder(order._id, { order: products, orderData: orderForm })

    this.setState({ loading: false, showModal: false, orderForm: {
      name: '',
      phone: '',
      comment: ''
    } })

  }


  clearOrderFormFields = () => {

    this.setState({
      orderForm: {
        name: '',
        phone: '',
        comment: ''
      }
    })

  }


  handleProductQuantityChange = (val, productId) => {

    clearTimeout(this.state.updateTimer)

    // console.log('Product %s quantity %s', productId, val);

    const { products } = this.state;

    const newProducts = products.map(prod => {

      if (productId === prod.product._id) prod.quantity = val;

      return { ...prod };

    })

    this.setState({
      products: [...newProducts],
      updateTimer: setTimeout(this.updateProducts, 2000)
    })

  }


  handleDeliveryChange = e => {

    e.preventDefault();

    const { delivery } = this.state;
    const index = Number(e.target.value);


    const newDelivery = delivery.map((del, ind) => {

      let temp = { ...del }

      index === ind ? temp.chosen = true : temp.chosen = false;

      return temp;

    })


    this.setState({ delivery: [...newDelivery] })

  }


  handleRemoveProduct = async productId => {

    clearTimeout(this.state.updateTimer)

    const newProducts = this.state.products.filter(prod => prod.product._id !== productId)

    await this.props.updateCurrentOrder(this.props.cart.order._id, newProducts)

    const { order } = this.props.cart;
    const products = order ? order.content : [];

    this.setState({
      products: [...products]
    })

  }


  updateProducts = async () => {

    // if (this.state.loading) return;

    // this.setState({ loading: true })


    const { products } = this.state;
    // const { content } = this.props.cart.order;



    // UPDATE REQUEST
    await this.props.updateCurrentOrder(this.props.cart.order._id, products)

    // this.setState({ loading: false })

  }


  handleModalToggleVisibility = (state) => this.setState({ showModal: state === 'show' ? true : false })



  render() {

    const { products, delivery, loading, showModal, orderForm } = this.state;

    if (loading || !this.props.dataFetched) {

      return <div className="content"><LoadingScreen /></div>

    }

    if (!products.length)
      return <div className="content">
        <div className="empty-cart">
          <div className="empty-cart__alert">
            <span>В корзине нет товаров</span>
            <Link to="/products" className="btn btn-light mt-3"><FaReply className="mr-2" />вернуться в продукты</Link>
          </div>
        </div>
      </div>;

    const noSubmit = orderForm.name.length < 4 || orderForm.phone.length < 10;

    let totalPrice = 0;
    const deliveryCost = delivery.find(del => del.chosen).price;

    const tableRows = products.map((prod, ind) => {

      totalPrice += Number(prod.product.price.value) * Number(prod.quantity);

      console.log(`Product #${ind + 1}: `, prod);

      //link with prodname and maybe photo
      const prodCell = <td className="cart__product-info">
        <div className="row product-info_row">
          <img src={prod.product.image} alt={prod.product.title} />
          <div className="prod_links">
            <h3><Link className="cart-product__title" to={`/products/${prod.product.slug}`}>{prod.product.title}</Link></h3>
            <button className="btn btn-link btn_remove" onClick={() => this.handleRemoveProduct(prod.product._id)}><FaTrash className="btn_remove mr-1" />удалить товар</button>
          </div>
        </div>
      </td>;

      return (
        <tr className="cart-item" key={ind + 55}>
          <th scope="row">{ind + 1}</th>
          {prodCell}
          <td>{prod.product.price.value}</td>
          <td><ProductQuantity productId={prod.product._id} quantity={prod.quantity} handleChangeQuantity={this.handleProductQuantityChange} /></td>
          <td className="cart-product__price-sum">{prod.quantity * prod.product.price.value}</td>
        </tr>
      )

    })

    const deliveryMethods = delivery.map((del, index) => <option className="form-control" value={index} key={index + 77}>{del.type}</option>)



    return (
      <div className="content">
        { this.props.processingRequest && <LoadingScreen /> }

        <div className="container">

          <Modal
            backdrop="static" centered show={showModal}
            onHide={() => this.handleModalToggleVisibility('close')}
          >

            <Modal.Header closeButton>
              <Modal.Title className="px-3" id="contained-modal-title-vcenter">Оформление заказа</Modal.Title>
            </Modal.Header>

            <Modal.Body className="m-auto">

              <Alert variant="warning">
                Необходимо указать Ваше имя и контактные данные. Любые вопросы по заказу Вы можете задать нашему менеджеру, который с Вами свяжется после оформления заказа<br /><hr />
                <b>Ваш заказ: </b>{products.map((prod, ind) => `${prod.product.title} x ${prod.quantity}`)}<br /><br />
                <b>Сумма заказа: </b>{totalPrice} руб.
              </Alert>
              <Form onSubmit={ this.handleOrderSubmit }>

                <Form.Group controlId="name">

                  <Form.Label>Ваше имя:</Form.Label>
                  <Form.Control type="text" onChange={this.handleFormFieldChange} value={orderForm.name} />
                  <Form.Text className="text-muted">* обязательно для заполнения</Form.Text>

                </Form.Group>
                <Form.Group controlId="phone">

                  <Form.Label>Ваш телефон:</Form.Label>
                  <Form.Control type="phone" onChange={this.handleFormFieldChange} value={orderForm.phone} />
                  <Form.Text className="text-muted">* обязательно для заполнения</Form.Text>

                </Form.Group>
                <Form.Group controlId="comment">

                  <Form.Label>Комментарий:</Form.Label>
                  <Form.Control
                    type="textarea" as="textarea"
                    value={orderForm.comment}
                    onChange={this.handleFormFieldChange}
                  />

                </Form.Group><br />
                <Form.Group className="text-right">
                  <Button 
                    variant="outline-secondary" className="mr-2"
                    onClick={ this.clearOrderFormFields }
                  ><FaEraser className="mr-2" />Очистить</Button>
                  <OverlayTrigger 
                    show={ noSubmit }
                    overlay={<Tooltip id="tooltip-disabled">
                      { noSubmit ? 'Нужно заполнить обязательные поля формы, чтобы кнопка была активна' : 'отправить заказ менеджеру' }
                    </Tooltip>}
                  ><span className="d-inline-block">
                    <Button type="submit" variant="primary" disabled={noSubmit} style={{ pointerEvents: noSubmit ? 'none' : 'initial' }}>
                      { loading ? <FaSpinner className="mr-2 icon-spin" /> : <FaPaperPlane className="mr-2" /> }Отправить
                    </Button>
                  </span>
                  </OverlayTrigger>

                </Form.Group>


              </Form>

            </Modal.Body>

          </Modal>

          <div className="row">

            <div className="col-md-8">

              <div className="shopping-cart shadow-sm">

                <table className="table table-striped table-responsive">

                  <thead className="text-center">

                    <tr>
                      <th scope="col-md-1">#</th>
                      <th scope="col-md-5">Товар</th>
                      <th scope="col-md-2">Цена</th>
                      <th scope="col-md-2">Кол-во</th>
                      <th scope="col-md-2">Сумма</th>
                    </tr>

                  </thead>

                  <tbody>{tableRows}</tbody>

                </table>

              </div>

            </div>
            <div className="col-md-4">

              <div className="shopping-cart__details shadow-sm">

                <h2 className="cart-details__title">Детали заказа</h2>
                <div className="alert cart_alert">

                  <small>Нажимая кнопку "оформить заказ", вы соглашаетесь с условиями публичной оферты</small>

                </div>

                <ul className="cart-details__list">

                  <li className="cart-details__line">

                    Доставка:
                  <span className="ml-2 cart-details__value">
                      <select
                        name="delivery" id="delivery"
                        onChange={this.handleDeliveryChange}
                        value={delivery.findIndex(del => del.chosen)}
                      >{deliveryMethods}</select>
                    </span>

                  </li>
                  <li className="cart-details__line">Стоимость доставки:<span className="cart-details__value">{deliveryCost}&nbsp;<FaRubleSign /></span></li>
                  <li className="cart-details__line">Товаров в корзине:<span className="cart-details__value">{products.length}&nbsp;шт.</span></li>
                  <li className="cart-details__line">Сумма заказа:<span className="cart-details__value">{totalPrice}&nbsp;<FaRubleSign /></span></li>
                  <li className="cart-details__line cart_total">Итого:<span className="cart-details__value ml-2">{totalPrice + deliveryCost}&nbsp;<FaRubleSign /></span></li>

                </ul>
                <button className="btn-block btn btn-primary" onClick={() => this.handleModalToggleVisibility('show')}>Оформить заказ</button>
                <Link to="/products" className="btn-block btn btn-light">Продолжить покупки</Link>

              </div>

            </div>

          </div>

        </div>

      </div>
    )

  }
}

CartPage.propTypes = {

}

const mapStateToProps = ({ cart, page }) => ({ cart, dataFetched: page.initialDataFetched, processingRequest: page.loading })

const mapDispatchToProps = { updateCurrentOrder, submitOrder }

export default connect(mapStateToProps, mapDispatchToProps)(CartPage)
