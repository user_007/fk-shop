import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { FaCubes, FaEnvelope, FaWallet } from "react-icons/fa";

import LoadingScreen from '../../components/loading-screen'
import ProductRow from '../../components/product-row';

import { loadPageData } from '../../actions/dataActions'

import "./style.css";

class AdminMainPage extends Component {
  static propTypes = {
    prop: PropTypes,
  };

  state = { loading: true }


  async componentDidMount() {

    await this.props.loadPageData(this.props.history)
    this.setState({ loading: false })

  }

  handleRemoveProduct = productId => {

    console.log(`Removing product with ID: ${productId}`);

  }

  render() {

    console.log('ADMIN PROPS: ', this.props.page)
    
    if (this.state.loading || this.props.page.loading || !this.props.page.initialDataFetched) 
    return <div className="content"><LoadingScreen /></div>;

    const { content: { stats, products } } = this.props.page;

    const productsRows = products.map( 
      (prod, ind) =>  <ProductRow key={ ind } { ...prod } onRemove={ this.handleRemoveProduct } />
    )

    return (
      <div className="content">
        <div className="container">
          <div className="row">
            <div className="col-md-3"></div>
            <div className="col-md-9">
              <div className="row">
                <div className="col-md-4">
                  <div className="card stat_card">
                    <div
                      class="card-body d-flex"
                      style={{ backgroundColor: "#398bf7" }}
                    >
                      <div class="d-flex">
                        <div class="mr-3 align-self-center">
                          <FaCubes className="card__stat-icon" />
                        </div>
                        <div class="align-self-center">
                          <h6 class="text-white card__stat-title mt-2">
                            Всего продуктов
                          </h6>
                          <p class="m-0 text-white card__stat-text">
                            {`${stats.products} позиций`}
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-4">
                  <div className="card stat_card">
                    <div
                      class="card-body d-flex"
                      style={{ backgroundColor: "#06d79c" }}
                    >
                      <div class="d-flex">
                        <div class="mr-3 align-self-center">
                          <FaWallet className="card__stat-icon" />
                        </div>
                        <div class="align-self-center">
                          <h6 class="card__stat-title text-white mt-2">
                            Всего заказов
                          </h6>
                          <p class="m-0 text-white card__stat-text">
                            {/* <small>в процессе: 3</small> */}
                            <small>{`завершенных: ${stats.orders}`}</small>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-md-4">
                  <div className="card stat_card">
                    <div
                      class="card-body d-flex"
                      style={{ backgroundColor: "#ef5350" }}
                    >
                      <div class="d-flex">
                        <div class="mr-3 align-self-center">
                          <FaEnvelope className="card__stat-icon" />
                        </div>
                        <div class="align-self-center">
                          <h6 class="card__stat-title text-white mt-2">
                            Обратная связь
                          </h6>
                          <p class="m-0 text-white card__stat-text">
                            {`${stats.notifications} сообщений`}
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              {/* products table */}
              <div className="row mt-3">
                <div className="col-12">
                  <div class="card">
                    <div class="card-body">
                      <div class="d-flex align-items-center">
                        <div className="mb-3">
                          <h5 class="card-title">Таблица товаров</h5>
                          <h6 class="card-subtitle">для сортировки товаров нажмите на заголовок столбца, по которому хотите сделать сортировку</h6>
                        </div>
                        {/* <div class="ml-auto d-flex no-block align-items-center">
                          <div class="dl">
                            <span>Сортировка: </span>
                            <select class="custom-form-control form-control">
                              <option value="0">Monthly</option>
                              <option value="1">Daily</option>
                              <option value="2">Weekly</option>
                              <option value="3">Yearly</option>
                            </select>
                          </div>
                        </div> */}
                      </div>
                      <div class="table-responsive">
                        <table class="no-wrap v-middle table">
                          <thead>
                            <tr class="border-0">                              
                              <th class="border-0"></th>
                              <th class="border-0"></th>
                              <th class="border-0"></th>
                              <th class="border-0"></th>
                              <th class="border-0"></th>
                            </tr>
                          </thead>
                          <tbody>{productsRows}</tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}


const mapStateToProps = ({ page }) => ({ page })

const mapDispatchToProps = {
  loadPageData
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminMainPage);
