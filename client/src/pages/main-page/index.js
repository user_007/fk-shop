import React, { Component } from "react";
import { connect } from 'react-redux'

import { loadPageData } from '../../actions/dataActions'

import MainMenu from '../../components/main-menu'
import ProductSlider from "../../components/slider-imitation";
import LoadingScreen from '../../components/loading-screen'

import slide from "../../assets/images/slide-for-site.jpg";

import { getMenuItems } from '../../utils'

import "./style.css";

class MainPage extends Component {

    state =  {
        loading: true
    }


    async componentDidMount() {

        await this.props.loadPageData(this.props.history)

        this.setState({ loading: false })

    }



  render() {

    const { content, initialDataFetched, brands, categories } = this.props.page;

    if (this.state.loading || !initialDataFetched) return <div className="content"><LoadingScreen /></div>;

    const productsSliders = content.map(el => {

        const products = el.products.map(prod => ({ ...prod, brand: { name: el.brand_name } }))

        return <ProductSlider
        title={`Продукты ${el.brand_name}`} key={ el._id }
        products={products.slice(0, 3)}
        description={el.brand_desc}
      />

    })

    const mainMenuProps = window.innerWidth > 767 ?
      { disabled: true, show: true } : null

    return (
      <div className="content">

        <div className="container">
          {/* <Link to="/products/">Продукты</Link> */}
          <div className="row">

            <div className="col-md-3 mb-3">

              <MainMenu 
                loading={ !initialDataFetched } 
                items={ getMenuItems(brands, categories) }
                { ...mainMenuProps } 
              />
              
            </div>
            <div className="col-md-9">

              <div className="main-slider">

                <div className="slide">

                  <img className="slide__image" src={slide} alt="слайд" />

                </div>

              </div>
              {productsSliders}

            </div>

          </div>

        </div>

      </div>
    );
  }
}


const mapStateToProps = ({ page }) => ({ page })
const mapDispatchToProps = { loadPageData }


export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
