import React, { Component } from "react";
import PropTypes from "prop-types";
import { FaShoppingCart, FaSpinner } from "react-icons/fa";
import axios from "axios";

import Loading from "../../components/page-loading";
import Quantity from "../../components/product-quantity";
import PageTopline from "../../components/page-topline";

import { updateCurrentOrder } from "../../actions/cartActions";
import { showNotification } from "../../actions/dataActions";

import "./style.css";
import { connect } from "react-redux";

class ProductPage extends Component {
  state = {
    loading: true,
    product: null,
    quantity: 1,
    processingRequest: false,
  };

  static propTypes = {
    productId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  };

  async componentDidMount() {
    const { productSlug } = this.props.match.params;
    // eslint-disable-next-line
    // const prodData = products.find(product => product.id == productSlug)

    try {
      const response = await axios.get(`/api/v1/products/${productSlug}`);

      if (!response.data || response.data.status !== "success")
        return console.error("ERROR: ", response);

      const { data: prodData } = response.data;

      this.setState({
        loading: false,
        product: { ...prodData },
      });
    } catch (err) {
      console.error("Error requesting product:\n", err);
    }
  }

  handleAddToCart = async () => {
    const { showNotification, updateCurrentOrder, order } = this.props;
    const { product, quantity, processingRequest } = this.state;

    if (processingRequest) return;

    this.setState({ processingRequest: true });

    let orderId = order ? order._id : null;
    let orderData = order
      ? [...order.content, { product: product._id, quantity }]
      : [];

    if (order) {
      orderId = order._id;
      orderData = order.content.some((el) => el.product._id === product._id)
        ? order.content.map((el) => ({
            product: el.product,
            quantity:
              el.product._id === product._id
                ? el.quantity + quantity
                : el.quantity,
          }))
        : [...order.content, { product, quantity }];
    }

    await updateCurrentOrder(orderId, orderData);

    showNotification({
      text: "Товар успешно добавлен в корзину",
      type: "success",
    });
    this.setState({ processingRequest: false });
  };

  onQuantityChange = (value) => this.setState({ quantity: value });

  render() {
    const { loading, product, quantity, processingRequest } = this.state;

    if (loading) return <Loading />;

    const btnIcon = processingRequest ? (
      <FaSpinner className="mr-3 icon-spin" />
    ) : (
      <FaShoppingCart className="mr-3" />
    );

    const propertiesList = product.properties.map((prop, index) => {
      return (
        <li className="product__property" key={index}>
          <span className="prop-name">{prop.title}</span>
          <span className="prop-value">{prop.content}</span>
        </li>
      );
    });

    return (
      <div className="content">
        <div className="container">
          <PageTopline pageData={ this.props.page } title={ product.title } />
          <div className="product">
            <div className="row">
              <div className="col-md-6">
                <div className="product-image">
                  <img src={product.image} alt={product.title} />
                </div>
              </div>
              <div className="col-md-6">
                <div className="product__main-info">
                  <h2 className="product__name">{product.title}</h2>
                  <p className="product__short-desc">{product.purpose}</p>
                  <div className="row">
                    <div className="col-md-6">
                      <ul className="product__properties-list">
                        {propertiesList}
                      </ul>
                    </div>
                    <div className="col-md-6">
                      <div className="product__tools">
                        <div className="d-inline-flex align-items-center justify-content-center">
                          цена{" "}
                          <span className="product__price mx-2 font-weight-bold font-size-md">
                            {product.price.value}
                          </span>{" "}
                          руб.
                        </div>
                        {product.price.description && (
                          <small
                            className="text-muted"
                            style={{ marginTop: "-10px" }}
                          >
                            {product.price.description}
                          </small>
                        )}
                        <div className="product__quantity">
                          <Quantity
                            handleChangeQuantity={this.onQuantityChange}
                            quantity={quantity}
                          />
                        </div>
                        <button
                          className="btn btn_buy btn-block"
                          disabled={processingRequest}
                          onClick={this.handleAddToCart}
                        >
                          {btnIcon} в корзину
                        </button>
                      </div>
                    </div>
                  </div>
                  {/* <div className="row">

                                        <div className="col-md-6">

                                            <div className="product__tools">

                                                <div className="d-inline-flex align-items-center justify-content-center">
                                                    цена <span className="product__price mx-2 font-weight-bold font-size-md">{product.price.value}</span> руб.
                                                </div>
                                                <div className="product__quantity"><Quantity handleChangeQuantity={this.onQuantityChange} quantity={quantity} /></div>

                                            </div>

                                        </div>
                                        <div className="col-md-6">

                                            <button className="btn btn-primary">в корзину</button>

                                        </div>

                                    </div> */}
                </div>
              </div>
              <div className="col-md-12 mt-4">
                <h4>Описание товара</h4>
                <p>{product.description}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ cart, page }) => ({ order: cart.order, page });

const mapDispatchToProps = { updateCurrentOrder, showNotification };

export default connect(mapStateToProps, mapDispatchToProps)(ProductPage);
