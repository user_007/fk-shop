import ProductPage from './product-page';
import ProductsCatalog from './products-catalog';
import MainPage from './main-page'
import CartPage from './cart-page'
import ContactPage from './contact-page'
import LoadingPage from './loading-page'
import AuthPage from './auth-page'
import AdminMainPage from './admin-main'
import ManageProductPage from './manage-product'



export {
  ProductPage, ProductsCatalog, MainPage, CartPage, ContactPage, LoadingPage, AuthPage,
  AdminMainPage, ManageProductPage
}
