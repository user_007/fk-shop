import React, { Fragment, useState } from "react";
import { useHistory } from 'react-router-dom'
// import PropTypes from "prop-types";
import './style.css'


const AuthPage = () => {

  const [login, setLogin] = useState('')
  const [password, setPassword] = useState('')
  // const [login, setLogin] = useState('')

  let history = useHistory();


  const handleSubmit = e => {

    e.preventDefault();    

    history.push('/admin')

  }

  return (
    <Fragment>
      <div className="wrapper fadeInDown">
        <div className="auth-form shadow">
          <h2 className="active auth-form__title mb-3"> Вход в админку </h2>

          <form onSubmit={ handleSubmit }>
            <input
              type="text" name="login"
              className="fadeIn second auth-form__field"              
              placeholder="логин"
              value={ login } onChange={ e => setLogin(e.target.value) }
            />
            <input
              type="text" name="password"
              className="fadeIn third auth-form__field"              
              placeholder="пароль"
              value={ password } onChange={ e => setPassword(e.target.value) }
            />
            <input type="submit" className="fadeIn fourth auth-form__field mt-3" value="войти" />
          </form>

          <div className="auth-form__footer">
            <a className=" auth-form__restore-pwd underlineHover" href="#">
              Восстановление пароля
            </a>
          </div>
        </div>
      </div>
    </Fragment>
  );
};



AuthPage.propTypes = {};

export default AuthPage;
