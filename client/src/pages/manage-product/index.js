import React, { Component } from 'react'
import { connect } from 'react-redux'

import ProductForm from '../../components/product-form'

import { loadPageData } from '../../actions/dataActions'


class ManageProductPage extends Component {

  state = {
    loading: true
  }


  render() {
    return (
      <div className="content">
        <ProductForm />
      </div>
    )
  }
}


const mapStateToProps = ({ page }) => ({ page })

const mapDispatchToProps = {
  loadPageData
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageProductPage)
