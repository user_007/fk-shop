import React, { Component } from 'react'
import { connect } from 'react-redux'

import LoadingScreen from '../../components/loading-screen'

import { getStartupData } from '../../actions/dataActions'

class LoadingPage extends Component {
  
  state = {

    redirect: false

  }


  async componentDidMount() {

    if (!this.props.dataFetched) {

      await this.props.getStartupData()

    }

  }


  render() {

    const { dataFetched } = this.props

    if (!dataFetched) return <div id="fdp"><LoadingScreen /></div>

    return null;
  }
}



const mapStateToProps = ({ page }) => ({ dataFetched: page.initialDataFetched })

const mapDispatchToProps = {

  getStartupData

}

export default connect(mapStateToProps, mapDispatchToProps)(LoadingPage);
