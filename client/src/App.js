import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux'

import store from './store'

import { 
  ProductsCatalog as Catalog, ProductPage, MainPage, ContactPage, CartPage, LoadingPage,
  AuthPage, AdminMainPage, ManageProductPage
} from './pages';
import Header from './components/layout/page-header';
import AdminHeader from './components/layout/admin-header'
import Footer from './components/layout/page-footer';
import ToastMessages from './components/layout/toast-messages';

import './App.css';



class App extends Component {

  state = { loading: false }


  // componentDidMount() {

  //   getStartupData()
  //     .then( res => this.setState({ loading: false }) )
  //     .catch(err => console.error('START ERROR: ', err))

  // }

  render() {

    return (
      <Provider store={store}>
        <Router>

          <Route component={LoadingPage} />
          <Route component={ToastMessages} />
          <Switch>
            <Route path="/auth" component={ null } />
            <Route path="/admin" component={AdminHeader} />
            <Route component={Header} />
          </Switch>
          
          <Switch>
            <Route exact path='/' component={MainPage} />
            <Route exact path='/products/' component={Catalog} />
            <Route path='/products/:productSlug' component={ProductPage} />
            <Route path='/contacts' component={ContactPage} />
            <Route exact path='/cart' component={CartPage} />
            <Route path="/auth" component={ AuthPage } />
            <Switch>
              <Route exact path="/admin" component={ AdminMainPage } />
              <Route path="/admin/manage-product" component={ ManageProductPage } />
              <Route path="/admin/manage-product/:productId" component={ ManageProductPage } />
              <Route exact path="/admin/orders" />
              <Route path="/admin/orders/:orderId" />
              <Route path="/admin" component={ AdminMainPage } />
            </Switch>
          </Switch>
          <Switch>
            <Route path="/auth" component={null} />
            <Route path="/admin" component={null} />
            <Route component={Footer} />
          </Switch>
        </Router>
      </Provider>
    )

  }

}


export default App;
