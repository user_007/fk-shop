import shopCart from './shop-cart.ico'

export const products = [
  {
    id: 1,
    title: 'Клей для плитки',
    image: 'https://res.cloudinary.com/dshj8uydq/image/upload/v1560424567/k0j4tldio6kddhtv02jg.png',
    price: { value: '300', description: '' },
    brand: 'Felker',
    category: 'Клей',
    purpose: 'Предназначен для приклеивания плитки',
    description: 'Описание клея для плитки'
  },
  {
    id: 2,
    title: 'Штукатурка декоративная "Короед"',
    image: 'https://res.cloudinary.com/dshj8uydq/image/upload/v1560424567/k0j4tldio6kddhtv02jg.png',
    price: { value: '600', description: '' },
    brand: 'Felker',
    category: 'Штукатурка',
    purpose: 'Предназначена для декоративной отделки фасада',
    description: 'Описание декоративной штукатурки'
  },
  {
    id: 3,
    title: 'Скандик XL 100мм',
    image: 'https://shop.rockwool.ru/media/catalog/product/cache/1/small_image/220x/9df78eab33525d08d6e5fb8d27136e95/n/e/new_xl_1-min.png',
    price: { value: '840', description: '' },
    brand: 'Rockwool',
    category: 'Утеплитель',
    purpose: 'Предназначен для утепления балконов, каркасных стен, мансард, перегородок, перекрытий между этажами, полов над холодным подвалом, стен с отделкой сайдингом',
    description: 'Популярный продукт, предназначенный для утепления частного домостроения большого формата. Плиты формата 1200х600х100 удобны в применении, т.к. их использование сокращает время монтажа - одна плита закрывает большую площадь. Также материал формата XL удобен при транспортировке в крупногабаритном транспорте.'
  },
  {
    id: 4,
    title: 'Гидро-пароизоляция',
    image: 'https://shop.rockwool.ru/media/catalog/product/cache/1/image/475x360/040ec09b1e35df139433887a97daa66f/h/y/hydro_barrier.png',
    price: { value: '2300', description: '' },
    brand: 'Rockwool',
    category: 'Гидроизоляция',
    purpose: 'Предназначена для изоляции парных бань и саун, каркасных стены, кровли, скатной кровли',
    description: 'Гидро-пароизоляция ROCKWOOL® – двухслойный материал из высокопрочной полипропиленовой ткани с полипропиленовым ламинатом с одной стороны. Применяется в строительстве для защиты конструкции здания от проникновения из атмосферы влаги и конденсата.'
  },
  {
    id: 5,
    title: 'Аккустик Баттс',
    image: 'https://shop.rockwool.ru/media/catalog/product/cache/1/image/475x360/040ec09b1e35df139433887a97daa66f/a/c/acustic-front.jpg',
    price: { value: '1130', description: 'цена за м2 205 руб.' },
    brand: 'Rockwool',
    category: 'Звукоизоляция',
    purpose: 'Предназначен для звукоизоляции нежилых помещений, перегородок в жилых помещениях, перекрытий по лагам ',
    description: 'АКУСТИК БАТТС® - от шума и огня! Мешают голоса, музыка у соседей или звуки с улицы? Используйте АКУСТИК БАТТС® в любых перегородках в квартире, в офисном здании или гостинице.',
    properties: [
      { title: 'размер', content: '1000*600*50' },
      { title: 'количество в упаковке', content: '6шт' },
      { title: 'плотность', content: '35-45 кг/м3' },
      { title: 'класс пожароопасности', content: 'КМ-0' },
      { title: 'водопоглащение', content: '1 кг/м2' },
    ]
  },
  {
    id: 6,
    title: 'Клей для газобетона',
    image: 'https://res.cloudinary.com/dshj8uydq/image/upload/v1560424567/k0j4tldio6kddhtv02jg.png',
    price: { value: '215', description: '' },
    brand: 'Felker',
    category: 'Клей',
    purpose: 'Предназначен для приклеивания блоков пено- и газобетона',
    description: 'Кладочно-монтажный клей для наружных и внутренних работ предназначен для кладки стен и перегородок из блоков и плит на основе ячеистых бетонов (пенобетона и газобетона), газосиликата, силикатных блоков и плит. Возможно использование клея при заделке сколов и выбоин плит из газо- и пенобетона до 10 см.'
  }
]


export const categories = [ 'Клей', 'Штукатурка', 'Утеплитель', 'Гидроизоляция', 'Огнезащита', 'Звукоизоляция' ]

export const brands = [ 'Felker', 'Rockwool']

export const cartIcon = shopCart;

export const contacts = {
  phone1: '+7(978)888-19-37',
  phone2: '+7(978)888-19-37',
  email: 'office@fasadkrym.com',
}
