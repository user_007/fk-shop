import React from 'react'
import PropTypes from 'prop-types'

import { FaSpinner } from 'react-icons/fa'

import './style.css'


const LoadingScreen = ({ message }) => {
  return (
  <div className="loading-screen"><FaSpinner className="mr-2 icon-spin" />{ message }</div>
  )
}

LoadingScreen.propTypes = {
  message: PropTypes.string
}

LoadingScreen.defaultProps = {
  message: ''
}

export default LoadingScreen
