import React from "react";
import { Breadcrumb } from 'react-bootstrap'


const Breadcrumbs = ({ path, name }) => {

  const navigation = {
    products: { path: '/products', title: 'продукты' },
    cart: { path: '/cart', title: 'корзина' },
    contats: { path: '/contacts', title: 'контакты' }
  }


  let crumbsList = path.split('/')

  if (crumbsList.length <= 1)  return null;

  crumbsList.shift();
  
  
  return (

    <Breadcrumb>

      <Breadcrumb.Item href="/">Главная</Breadcrumb.Item>

      {crumbsList.map( (crumb, key) =>
        navigation[crumb] ?
          <Breadcrumb.Item 
            key={key} href={ navigation[crumb].path }
            active={ key + 1 === crumbsList.length }
          >{ navigation[crumb].title.toLocaleLowerCase() }</Breadcrumb.Item> 
          : 
          <Breadcrumb.Item active key={key}>{ name.toLocaleLowerCase() }</Breadcrumb.Item>
      )}

    </Breadcrumb>

  );
};


export default Breadcrumbs;
