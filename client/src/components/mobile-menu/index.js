import React, { Component } from 'react'
// import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { slide as Menu } from 'react-burger-menu'
import { FaHome, FaAddressBook, FaBoxes } from 'react-icons/fa'

import logo from '../../assets/images/logo.svg'

import './style.css'

class MobileMenu extends Component {

  state = {
    isOpen: false
  }

  handleClick = e => {

    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
    this.setState({ isOpen: false })
  }

  render() {

    const { isOpen } = this.state;

    if (window.innerWidth > 767) {
      
      return null;
    };

    return (
      <Menu isOpen={isOpen} onStateChange={state => this.setState({ isOpen: state.isOpen })} disableAutoFocus right >
        <Link to="/">
        <div className="bm-menu_header">
          <div className="bm-menu__logo"><img className="mw-100 mh-100" src={ logo } alt="логотип"/></div>
          <h3 className="bm-menu__title">Навигация</h3>
        </div></Link>
        <Link id="home" className="menu-item" to="/" onClick={this.handleClick}><FaHome className="mr-4" />Главная</Link>
        <Link id="products" className="menu-item" to="/products" onClick={this.handleClick}><FaBoxes className="mr-4" />Товары</Link>
        <Link id="contact" className="menu-item" to="/contacts" onClick={this.handleClick}><FaAddressBook className="mr-4" />Контакты</Link>
      </Menu>
    )

  }
}


MobileMenu.propTypes = {

}

export default MobileMenu;
