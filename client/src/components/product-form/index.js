import React, { Fragment } from "react";
import PropTypes from "prop-types";

import { Form, Row, Col, Button } from "react-bootstrap";
import { FaSave, FaDownload } from "react-icons/fa";

import "./style.css";

const ProductForm = ({ title }) => {
  const handleSubmit = (e) => console.log("Prod Form submitted!");

  return (
    <Fragment>
      {/* <div className="w-100">
        <div className="product-form shadow">
          <h2 className="active auth-form__title mb-3"> Добавление / Изменение Товара </h2>

          <form onSubmit={ handleSubmit }>
            <input
              type="text" name="title"
              className="fadeIn second auth-form__field"              
              placeholder="название товара"
              // value={ login } onChange={ e => setLogin(e.target.value) }
            />
            <input
              type="text" name="brand"
              className="fadeIn third auth-form__field"              
              placeholder="брэнд товара"
              // value={ password } onChange={ e => setPassword(e.target.value) }
            />
            <input
              type="text" name="category"
              className="fadeIn third auth-form__field"              
              placeholder="категория товара"
              // value={ password } onChange={ e => setPassword(e.target.value) }
            />
            <input type="submit" className="fadeIn fourth auth-form__field mt-3" value="Сохранить" />
          </form>

          <div className="auth-form__footer"></div>
        </div>
      </div> */}

      <div className="container">

        <div className="product-form-wrapper">

          <Form>

            <h4 className="product-form__caption">
              Добавление / Изменение Товара
            </h4>
            <Row>

              <Col md={4}>

                <div className="product-form__sidebar">

                  <div className="product-form__img-wrapper">

                    <img 
                      src="https://res.cloudinary.com/dshj8uydq/image/upload/v1560424567/k0j4tldio6kddhtv02jg.png"
                      alt="temporary img"
                      className="mw-100 mh-100"
                    />

                  </div>
                  <Button 
                    block variant="light" size="sm"
                    className="mt-2"
                  ><FaDownload className="mr-2" />Загрузить фото</Button>
                  <Button 
                    block variant="primary" size="sm"
                    className="mt-2"
                  ><FaSave className="mr-2" />Сохранить</Button>
                  
                </div>

              </Col>
              <Col md={8}>
                <Form.Group>
                  <Form.Label>Название товара</Form.Label>
                  <Form.Control type="text" placeholder="Название товара" />
                  <Form.Text className="form-subtext">название товара</Form.Text>
                  {/* <div className="input-group input-group-icon">
                    <input type="text" placeholder="" />
                    <span className="input-icon">
                      <FaUserAlt />
                    </span>
                  </div> */}
                </Form.Group>

                <Form.Group disabled className="disabled">
                  <Form.Label>ЧПУ товара</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="добавляется автоматически"
                  />
                  <Form.Text className="form-subtext">добавляется автоматически</Form.Text>
                </Form.Group>
                

                <Row>

                  <Col xs="12" sm="6">
                    <Form.Group>
                      <Form.Label>Брэнд</Form.Label>
                      <Form.Control as="select" placeholder="брэнд">
                        <option className="select">Felker</option>
                        <option className="select">Rockwool</option>
                      </Form.Control>
                      <Form.Text className="form-subtext">брэнд товара</Form.Text>
                    </Form.Group>
                  </Col>
                  <Col xs="12" sm="6">
                    <Form.Group>
                      <Form.Label>Категория товара</Form.Label>
                      <Form.Control as="select" placeholder="категория">
                        <option>Теплоизоляция</option>
                        <option>Огнезащита</option>
                        <option>Клеевая смесь</option>
                      </Form.Control>
                      <Form.Text className="form-subtext">категория товара</Form.Text>
                    </Form.Group>
                  </Col>     
                  
                </Row>
                <Form.Group>

                  <Form.Label>Описание товара</Form.Label>
                  <Form.Control rows="6" as="textarea"></Form.Control>

                </Form.Group>

                <div className="product-form__section-title">Характеристики товара</div>
                <Form.Group>
                  <Row>
                    <Col xs="8">

                    <Form.Label>Название</Form.Label>
                    <Form.Control type="text"></Form.Control>

                    </Col>
                    <Col xs="4">

                      <Form.Label>Значение</Form.Label>
                      <Form.Control type="text"></Form.Control>

                    </Col>
                  </Row>
                </Form.Group>


              </Col>
            </Row>
          </Form>
        </div>
      </div>
      {/* .input-group.input-group-icon
        input(type="email", placeholder="Email Adress")
        .input-icon
          i.fa.fa-envelope
      .input-group.input-group-icon
        input(type="password", placeholder="Password")
        .input-icon
          i.fa.fa-key
    .row
      .col-half
        h4 Date of Birth
        .input-group
          .col-third
            input(type="text", placeholder="DD")
          .col-third
            input(type="text", placeholder="MM")
          .col-third
            input(type="text", placeholder="YYYY")
      .col-half
        h4 Gender
        .input-group
          input(type="radio", name="gender", value="male", id="gender-male")
          label(for="gender-male") Male
          input(type="radio", name="gender", value="female", id="gender-female")
          label(for="gender-female") Female
    .row
      h4 Payment Details
      .input-group
        input(type="radio", name="payment-method", value="card", id="payment-method-card", checked="true")
        label(for="payment-method-card")
          span
            i.fa.fa-cc-visa
            | Credit Card
        input(type="radio", name="payment-method", value="paypal", id="payment-method-paypal")
        label(for="payment-method-paypal") 
          span
            i.fa.fa-cc-paypal
            | Paypal
      .input-group.input-group-icon
        input(type="text", placeholder="Card Number")
        .input-icon
          i.fa.fa-credit-card
      .col-half
        .input-group.input-group-icon
          input(type="text", placeholder="Card CVC")
          .input-icon
            i.fa.fa-user
      .col-half
        .input-group
          select
            option 01 Jan
            option 02 Jan
          select
            option 2015
            option 2016
    .row
      h4 Terms and Conditions
      .input-group
        input(type="checkbox", id="terms")
        label(for="terms") I accept the terms and conditions for signing up to this service, and hereby confirm I have read the privacy policy. */}
    </Fragment>
  );
};

ProductForm.propTypes = {
  title: PropTypes.string,
};

export default ProductForm;
