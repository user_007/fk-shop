import React from 'react'
import { Link } from 'react-router-dom'
import logo from '../../assets/images/logo.svg'

import './style.css'


const LogoSection = props => {
  return (
    <Link to="/" className="header__info">
      <img src={logo} alt="логотип" className="brand__logo" />
      <div className="brand__text">
        <span className="brand__name">ФК-ШОП</span>
        <small className="brand__description">стройматериалы по низкой цене</small>
      </div>
    </Link>
  )
}

LogoSection.propTypes = {

}

export default LogoSection
