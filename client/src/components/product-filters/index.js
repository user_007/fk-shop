import React, { Fragment } from 'react'
import PropTypes from 'prop-types'


import './style.css'



const ProductFilters = ({ onFilterChange, filters }) => {


    const filtersBlock = filters.map(el => {

      return <Fragment>

        <h3 className="section_header">{el.title}</h3>
        {
          el.items.map((filter, ind) => <li className="catalog__item custom-control custom-checkbox" key={ind +5} style={{ padding: '0 10px' }} >
          <button 
            className={`${filter.active ? 'active ' : ''}btn btn-block btn_sort`}
            data-filter={ filter.name }
            onClick={ e => onFilterChange(filter.name) }
          >{filter.name}</button>
        </li>)
        }

      </Fragment>

    })



  return filtersBlock
}

ProductFilters.propTypes = {

  onFilterChange: PropTypes.func,
  filters: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.string,
    items: PropTypes.arrayOf(PropTypes.string)
  }))

}

export default ProductFilters
