import React from 'react'
import { FaSpinner } from 'react-icons/fa'

const Loading = () => {
    return (
        <div className="content row justify-content-center align-items-center font-weight-bold">
            <h4><FaSpinner className="icon-spin mr-2" />Загрузка...</h4>
        </div>
    )
}


export default Loading;
