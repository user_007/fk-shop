import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import { FaShoppingCart } from 'react-icons/fa'



const MiniCart = ({ products }) => {

  const numOfProducts = products.length;

  let badge = null, content = 0;

  if (numOfProducts > 0) {   

    products.forEach( (prod, ind) => {

      content += prod.quantity * 1 * prod.product.price.value

    })
    badge = <span className="basket-badge">{numOfProducts}</span>

  }

  return (

    <div className="shop-basket">

      <Link className="btn-shop-basket" to="/cart" >

        <FaShoppingCart className="basket-icon" />
        <span className="basket-price">{content > 0 ? content + 'р.' : 'нет\u00A0товаров'}</span>{badge}        

      </Link>

    </div>

  )
}

MiniCart.propTypes = {
  products: PropTypes.array

}

MiniCart.defaultProps = {
  products: []
}

export default MiniCart
