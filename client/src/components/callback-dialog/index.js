import React, { useState } from 'react'
import { Dropdown } from 'react-bootstrap'
import PropTypes from 'prop-types'

import { FaSpinner, FaPaperPlane } from 'react-icons/fa'

import './style.css'


const CallbackDialog = ({ handleRequestCallback }) => {


  const [ name, setName ] = useState('')
  const [ phone, setPhone ] = useState('')
  const [ loading, setLoading ] = useState(false)


  const sendRequest = async e => {

    e.preventDefault()

    if (loading) return;

    setLoading(true);

    await handleRequestCallback(name, phone);

    setLoading(false);
    setName('');
    setPhone('');

  }


  const icon = loading ? <FaSpinner className="mr-2 icon-spin" /> : <FaPaperPlane className="mr-2" />


  return (

    <Dropdown>

      <Dropdown.Toggle variant="link" className="callback_link" >заказать звонок</Dropdown.Toggle>
      <Dropdown.Menu alignRight className="callback_dropdown" >


          <form action="post" className="callback-form form" onSubmit={ sendRequest }>

            <h4 className="form-title">Обратный звонок</h4>
            <div className="alert alert-warning"><p className="callback_warning">Время ожидания обратного звонка обычно составляет от 1 до 30 минут</p></div>
            <div className="form-group">

              <input 
                name="name" id="name" type="text"
                className="form-control form-control-sm"
                disabled={ loading }
                placeholder="ваше имя..." value={ name }
                onChange={ e => { if (!loading) setName(e.target.value) } }
              />

            </div>
            <div className="form-group">

              <input 
                name="phone" id="phone" type="text" 
                className="form-control form-control-sm"
                disabled={ loading }
                placeholder="ваш телефон..." value={ phone }
                onChange={ e => { if (!loading) setPhone(e.target.value) } }
              />

            </div>
            <div className="form-group mt-4">

              <button 
                className="btn btn-block btn-sm btn-outline-primary"
                disabled={ loading }
              >{icon}заказать</button>

            </div>

          </form>


      </Dropdown.Menu>

    </Dropdown>
  )
}


CallbackDialog.propTypes = {

  handleRequestCallback: PropTypes.func

}


export default CallbackDialog
