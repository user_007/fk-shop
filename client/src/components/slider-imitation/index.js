import React from 'react'
import PropTypes from 'prop-types'
import ProductCard from '../product-card'

import './style.css'


const Slider = ({ products, title, description }) => {

  if ( products.length < 1 ) return null;

  const sliderContent = products.map(  (product, ind) => <ProductCard key={ ind } activeControl={ false } product={ product } /> )
  
  return (
    <div className="slider shadow-sm">
      <div className="slider__header">

        { title && <div className="slider__title">{ title }</div> }
        { description && <div className="slider__description">{ description }</div> }

      </div>
      <div className="row">{ sliderContent }</div>
    </div>
  )
}

Slider.propTypes = {
  products: PropTypes.arrayOf( PropTypes.object )
}


export default Slider;
