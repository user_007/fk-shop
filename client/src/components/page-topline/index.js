import React from 'react'
import PropTypes from 'prop-types'

import Breadcrumbs from '../breadcrumbs'
import MainMenu from '../main-menu'

import { getMenuItems } from '../../utils'

import './style.css'


const PageTopline = ({ title, menuIsStatic, pageData, children }) => {

  if (!pageData.initialDataFetched) return null;

  const menuItems = getMenuItems(pageData.brands, pageData.categories)

  return (
    <div className="page__top-line">

      <div className="row">

        <div className="col-md-3 col-md-3 align-self-center">
          {/* <button className="btn btn-block btn-primary"><FaBars className="mr-3" />Каталог товаров</button> */}
          { 
            menuIsStatic ?
            <MainMenu loading={ !pageData.initialDataFetched } items={ menuItems } disabled show={ true } />
            :
            <MainMenu loading={ !pageData.initialDataFetched } items={ menuItems }  />
          }
        </div>
        {
          title.length > 0 &&
          <div className="col-md-5">

          <div className="page_info">

            <h2 className="page__title">{title}</h2>
            <Breadcrumbs path={ document.location.pathname } name={ title }  />

          </div>
          

        </div>
        }
        <div className="col-md-4 align-self-center">{children}</div>

      </div>

    </div>
  )
}

PageTopline.propTypes = {
  title: PropTypes.string,
  child: PropTypes.any,
  menuIsStatic: PropTypes.bool
}

PageTopline.defaultProps = {
  title: '',
  menuIsStatic: false
}

export default PageTopline
