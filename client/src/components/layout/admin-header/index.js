import React from "react";
import { Link } from "react-router-dom";
import { FaEnvelope, FaWallet, FaCubes } from 'react-icons/fa'

import "./style.css";

const AdminHeader = () => {
  return (
    <header className="admin-header">
      <div className="container">
      <nav className="navbar navbar-expand-md navbar-dark">
        <div className="navbar-brand"><b>ФК-Шоп</b> Админка</div>

        <div className="collapse navbar-collapse">
          <ul className="navbar-nav ml-auto mt-2 mt-lg-0">
            <li className="nav-item active">
              <Link className="nav-link" to="/admin">
              <FaCubes className="mr-2" />продукты<span className="sr-only">(current)</span>
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/admin/orders">
              <FaWallet className="mr-2" />заказы
              </Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link" to="/admin/messages">
                <FaEnvelope className="mr-2" />сообщения
              </Link>
            </li>
          </ul>
        </div>
      </nav>
      </div>
    </header>
  );
};

export default AdminHeader;
