import React from 'react'
// import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { FaMobileAlt } from 'react-icons/fa'
import LogoSection from '../../logo-component'

import './style.css'


const Footer = () => {

  return (

    <div className="footer">

      <div className="container">

        <div className="row">

          <div className="col-md-4 footer__section">

            <LogoSection />
            <p className="info-block">Интернет-магазин ФК-Шоп реализует продажу и доставку качественных строительных материалов, таких как: теплоизоляционные и звукоизоляционные плиты, сухие строительные смеси для приклевания пенополистирольных плит, минеральной ваты и газобетона, штукатурки и другие материалы по всему Крыму и Городу Севастополю.</p>

          </div>
          <div className="col-md-4 footer__section">

            <h4 className="footer-section-title">Контакты</h4>
            <ul className="contact-data-list">

              <li className="contact-item">Пункт выдачи: г. Севастополь, Фиолентовское шоссе 18а</li>
              <li className="contact-item">График работы: все дни недели c 9.00 до 19.00</li>
              <li className="contact-item phone"><a href="tel:+79788881937"><FaMobileAlt className="mr-1" />+7(978) 888-19-37</a></li>

            </ul>
            <ul className="contact-data-list">

              <li className="contact-item">Офис: г. Севастополь, ул. Вакуленчука 29, офис 418</li>
              <li className="contact-item">График работы: все дни недели c 9.00 до 19.00</li>
              <li className="contact-item phone"><a href="tel:+79788881937"><FaMobileAlt className="mr-1" />+7(978) 888-19-37</a></li>

            </ul>


          </div>
          <div className="col-md-4 footer__section">

            <h4 className="footer-section-title">Информация</h4>
            <ul className="info-list">

              <li><Link className="info_link" to="/">О компании</Link></li>
              <li><Link className="info_link" to="/">Доставка и оплата</Link></li>
              <li><Link className="info_link" to="/">Договор оферты</Link></li>
              <li><Link className="info_link" to="/">Часто задаваемые вопросы</Link></li>

            </ul>

          </div>

        </div>

      </div>

    </div>

  )
}

Footer.propTypes = {

}


export default Footer;
