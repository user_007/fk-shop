import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Toast } from 'react-bootstrap'

import { closeNotification } from '../../../actions/dataActions'

import './style.css'


class Notification extends Component {


  // static getDerivedStateFromProps()


  render() {

    const { show = false, type, text, title } = this.props.notification;

    return (
      <Toast 
        onClose={() => this.props.closeNotification()} 
        show={show} delay={5000} autohide
        className="toast-message"
      >
          <Toast.Header className={`toast_${type}`}>
            <img
              src="placehold.it/20x20"
              className="rounded mr-2"
              alt=""
            />
            <strong className="mr-auto">{title}</strong>
            {/* <small>11 mins ago</small> */}
          </Toast.Header>
          <Toast.Body>{text}</Toast.Body>
        </Toast>
    )
  }
}


const mapStateToProps = ({ notification }) => ({ notification })

const mapDispatchToProps = { closeNotification }

export default connect(mapStateToProps, mapDispatchToProps)(Notification);
