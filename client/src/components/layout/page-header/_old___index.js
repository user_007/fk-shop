import React from 'react'
// import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { FaBars } from 'react-icons/fa'

import './style.css'


const Header = props => {
  return (
    
    <header>
        <div>
          <div className="container">
            <div className="row header__info">
              <Link to="/" className="col-md-3">
                <img src="http://www.placehold.it/40x40" alt="логотип" className="brand__logo" />
                <span className="brand__name">Название</span>
              </Link>
              <div className="col-md-3">
                <p className="slogan">Только качественные строительные материалы от надежных производителей</p>
              </div>
            </div>
          </div>
        </div>

        <nav className="navbar navbar-expand-lg navbar-light">
          <div className="container">
            {/* <a className="navbar-brand" href="#">Navbar</a>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button> */}
          <div className="row w-100">
            <div className="col-md-3">
              <button className="btn btn-block btn-primary"><FaBars className="mr-3" /> Каталог товаров</button>
            </div>
            <div className="col-md-9">
            <div className="collapse navbar-collapse nav-line" id="navbarTogglerDemo02">
            <ul className="navbar-nav mr-auto mt-2 mt-lg-0 w-60">
              <li className="nav-item main-menu_item active">
                <Link className="nav-link" to="/">Оплата <span className="sr-only">(current)</span></Link>
              </li>
              <li className="nav-item main-menu_item">
                <Link className="nav-link" to="/">Доставка</Link>
              </li>
              <li className="nav-item main-menu_item">
                <Link className="nav-link" to="/">О нас</Link>
              </li>
              <li className="nav-item main-menu_item">
                <Link className="nav-link" to="/">Контакты</Link>
              </li>
            </ul>
            <form className="form-inline my-2 my-lg-0">
              <input className="form-control mr-sm-2" type="search" placeholder="Поиск..." />
              <button className="btn btn-outline-dark my-2 my-sm-0" type="submit">Поиск</button>
            </form>
          </div>
            </div>
          </div>

          
          </div>
        </nav>
    </header>

  )
}

Header.propTypes = {

}

export default Header;
