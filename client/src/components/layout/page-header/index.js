import React, { Component } from 'react'
// import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import MobileMenu from '../../mobile-menu';
import { FaMobileAlt } from 'react-icons/fa'

import { getStartupData, sendCallBack } from '../../../actions/dataActions'

import LogoSection from '../../logo-component'
import CallbackDialog from '../../callback-dialog'
import MiniCart from '../../mini-cart'

import './style.css'


class Header extends Component {

  state = { 
    callBackProcessing: false 
  }


  // async componentDidMount() {

  //   await this.props.getStartupData()

  // }


  handleRequestCallback = async (name, phone) => {
    
    await this.props.sendCallBack({ name, phone })

  }


  render() {

    const order = this.props.cart.order ? this.props.cart.order.content : []


    return (

      <header>
        <MobileMenu />
        <nav className="navbar navbar-expand-lg navbar-light">
          <div className="container">
          
            <div className="row align-items-center w-100">
              <div className="col-md-3 col-8"><LogoSection /></div>
                  {/* <span className="slogan navbar-text">Только качественные строительные материалы от надежных производителей</span> */}
              <div className="col-md-6 hidden-sm">
                <div className="row">                
                <div className="collapse navbar-collapse nav-line" id="site-nav">
                  <ul className="navbar-nav mr-auto mt-2 mt-lg-0 w-60">
                    <li className="nav-item main-menu_item active">
                      <Link className="nav-link" to="/">Главная <span className="sr-only">(current)</span></Link>
                    </li>
                    <li className="nav-item main-menu_item">
                      <Link className="nav-link" to="/products">Товары</Link>
                    </li>
                    <li className="nav-item main-menu_item">
                      <Link className="nav-link" to="/contacts">Контакты</Link>
                    </li>
                  </ul>
                  <div className="contact-info">
                    <span className="nav-text"><FaMobileAlt className="mr-2" />+7(978) 555-55-55</span>
                    <span className="callback">
                      <CallbackDialog 
                        loading={ this.state.callBackProcessing } 
                        handleRequestCallback={ this.handleRequestCallback }
                    /></span>
                  </div>    
                </div>
                </div>
              </div>
              <div className="col-md-3 col-4">
              <MiniCart products={ order } />
              </div>
            </div>
  
  
          </div>
        </nav>
      </header>
  
    )

  }
}

Header.propTypes = {

}



const mapStateToProps = ({ page, cart }) => ({ page, cart })

const mapDispatchToProps = {

  getStartupData, sendCallBack

}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
