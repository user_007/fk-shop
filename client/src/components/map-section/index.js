import React from 'react';
import PropTypes from 'prop-types';


import { FaUserClock, FaMapMarkerAlt, FaEnvelopeOpenText, FaMobileAlt } from 'react-icons/fa';

import './style.css';


const MapSection = ({ withTabs }) => {
  return (
    <div className="map-container">
      <iframe className="map" title="расположение офиса и склада ФК Снаб" src="https://yandex.ru/map-widget/v1/?um=constructor%3Ae9350af6c527591c87a929174c73a37e4e00c337a78eddc035f013e33735160a&amp;source=constructor" width="1280" height="720" frameborder="0"></iframe>
      {
        withTabs && <div className="org-data">
        <div className="org-data--item" data-aos-offset="150" data-aos="fade-up">
          <h3 className="org-data--title"><FaMapMarkerAlt className="org-data--ico" /> Адрес:</h3>
          <p className="org-data--content">г. Севастополь, Фиолентовское шоссе 18а</p>
        </div>
        <div className="org-data--item" data-aos-offset="300" data-aos="fade-up">
          <h3 className="org-data--title"><FaEnvelopeOpenText className="org-data--ico" /> Эл. почта:</h3>
          <p className="org-data--content">office@fasadkrym.ru</p>
        </div>
        <div className="org-data--item" data-aos-offset="450" data-aos="fade-up">
          <h3 className="org-data--title"><FaMobileAlt className="org-data--ico" /> Телефон:</h3>
          <p className="org-data--content">+7(978)888-19-37</p>
          <p className="org-data--content">+7(978)888-19-37</p>
        </div>
        <div className="org-data--item" data-aos-offset="600" data-aos="fade-up">
          <h3 className="org-data--title"><FaUserClock className="org-data--ico" /> Время работы:</h3>
          <p className="org-data--content">пн.-пт.: 8:00 - 20:00</p>
        </div>
      </div>
      }
    </div>
  )
}

MapSection.propTypes = {
  withTabs: PropTypes.bool.isRequired
}

MapSection.defaultProps = {
  withTabs: false
}

export default MapSection;
