import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

import { FaShoppingBasket, FaCheck } from 'react-icons/fa'

import './style.css'


const ProductCard = ({ product, isAddedToCart, handleOnclick, activeControl }) => {

  const price = new Intl.NumberFormat('ru-Ru', { style: 'currency', currency: 'RUB' }).format(product.price.value)

  const productPagePath = `/products/${product.slug}`;

  const button = !activeControl ? null
    : isAddedToCart ?
    <button 
      disabled={ isAddedToCart } className="btn btn-outline-primary btn-sm "
    ><FaCheck className="mr-2" />в корзине</button>
    :
    <button 
      className="btn btn_buy btn-sm" onClick={ () => handleOnclick(product._id) }
    ><FaShoppingBasket />купить</button>
    

  return (
    <div className="col-md-4 col-sm-6 col-xs-12">
      <div className="product-card">
        <div className="product__marks">
          {/* for future discount logic */}
          {/* <span className="discount">-25%</span> */}
        </div>
      <Link to={ productPagePath } className="image">
        <img className="mw-100 mh-100" src={ product.image } alt={ product.title } />
      </Link>
      <p className="product__brand">{ product.brand.name }</p>
      <Link to={ productPagePath } className="product__title">{ product.title }</Link>
      <div className="card-tools">
        <div className="product__price">
          <span className="price">{ price }</span>
        </div>
        {/* <button className="btn btn_buy btn-sm"><FaShoppingBasket />купить</button> */}
        { button }
      </div>
      {/* <p className="product__quantity">Наличие: в наличии</p> */}
      </div>
    </div>
  )
}

ProductCard.propTypes = {
  product: PropTypes.shape({
    price: PropTypes.shape({ 
      value: PropTypes.number,
      description: PropTypes.string
    }),
    title: PropTypes.string,
    image: PropTypes.string,
    slug: PropTypes.string,
    brand: PropTypes.shape({ name: PropTypes.string })
  }),
  isAddedToCart: PropTypes.bool,
  handleOnclick: PropTypes.func,
  activeControl: PropTypes.bool
}

ProductCard.defaultProps = {
  isAddedToCart: false,
  activeControl: true
}

export default ProductCard
