import React from "react";
import PropTypes from "prop-types";
import { Link } from 'react-router-dom'
import { FaTrashAlt, FaEdit } from 'react-icons/fa'

import './style.css'

const ProductRow = ({ title, price, _id, image, brand, category, onRemove }) => {


  const handleRemove = e => {

    e.preventDefault();
    const confirmation = window.prompt('Напишите \"удалить\" для подтверждения!') === 'удалить'

    if (confirmation) onRemove(_id)

  }

  return (
    <tr className="product-row">
      <td>
        <div className="d-flex no-block align-items-center">
          <div className="mr-2">
            <img
              src={ image }
              alt="упаковка"
              className="rounded-circle"
              width="45"
            />
          </div>
          <div className="">
            <h5 className="mb-0 font-14 font-medium">{ title }</h5>
          </div>
        </div>
      </td>
      <td>{brand.name}</td>
      <td>{category.name}</td>
      <td>{price.value}</td>
      <td className="blue-grey-text  text-darken-4 font-medium">

        <button 
          className="btn btn-sm row-control_btn"
          onClick={ handleRemove }
        ><FaTrashAlt /></button>
        <Link 
          className="btn btn-sm row-control_btn"
          to={ `/admin/manage-product/${_id}` }
        ><FaEdit /></Link>

      </td>
    </tr>
  );
};

ProductRow.propTypes = {
  title: PropTypes.string,
  price: PropTypes.shape({ value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]) }),
  _id: PropTypes.string,
  image: PropTypes.string,
  brand: PropTypes.shape({ name: PropTypes.string }),
  category: PropTypes.shape({ name: PropTypes.string }),
  onRemove: PropTypes.func
};

ProductRow.defaultProps = {
  image: 'http://placehold.it/45x45',
  _id: '55555',
  title: 'Название товара',
  brand: { name: 'брэнд' },
  category: { name: 'категория' },
  price: { value: '420' }
}

export default ProductRow;
