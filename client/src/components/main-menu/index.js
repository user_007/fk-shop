import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { FaBars, FaSpinner } from 'react-icons/fa'
import { Dropdown } from 'react-bootstrap'

import './style.css'


const MainMenu = ({ items, loading, ...otherProps }) => {
  
  console.log('Menu data: ', items);

  let menuBody = loading ? 
    <Dropdown.Item disabled as="span" className="shop-category text-center"><FaSpinner className="icon-spin" /></Dropdown.Item>
    :
    <Fragment>
      <Dropdown.Item as="span" className="shop-category">

        <Link to={{
            pathname: "/products",
            state: {}
          }}>Все товары</Link>

        </Dropdown.Item>
      {
        items.length > 0 &&
        items.map(item =>
          <Dropdown.Item as="span" className="shop-category" key={ item.key }>
    
            <Link to={{
                pathname: "/products",
                state: item.linkState
            }}>{ item.name }</Link>
    
          </Dropdown.Item>
        )
      }
    </Fragment>


  return (

    <Dropdown { ...otherProps }>

      <Dropdown.Toggle variant="primary" className="w-100">
        
        <FaBars className="mr-3" />Каталог товаров

      </Dropdown.Toggle>
      <Dropdown.Menu className="shop-catalog">{ menuBody }</Dropdown.Menu>

    </Dropdown>

  )
}


MainMenu.propTypes = {

  items: PropTypes.array,
  loading: PropTypes.bool

}

MainMenu.defaultProps = {

  items: [],
  loading: false

}

export default MainMenu;
