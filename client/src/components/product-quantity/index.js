import React from 'react'
import PropTypes from 'prop-types'
import { FaChevronUp, FaChevronDown } from 'react-icons/fa'

import './style.css'


const ProductQuantity = ({ handleChangeQuantity, quantity, title, productId }) => {

  const addProduct = () => {

    return quantity < 99 ? quantity + 1 : quantity

  }

  const removeProduct = () => {

    return quantity > 1 ? quantity - 1 : quantity

  }


  return (
    <div className="product-quantity">

      { title && title }
      <input  type="text" value={ quantity } onChange={ e => console.log(e.target.value) } className="product-quantity__value" />
      <div className="product-quantity__control">

        <button className="btn btn-light btn_quantity" onClick={ () => handleChangeQuantity(addProduct(), productId) }><FaChevronUp /></button>
        <button className="btn btn-light btn_quantity" onClick={ () => handleChangeQuantity(removeProduct(), productId) }><FaChevronDown /></button>

      </div>

    </div>
  )
}

ProductQuantity.propTypes = {
  handleChangeQuantity: PropTypes.func,
  quantity: PropTypes.number,
  title: PropTypes.string,
  productId: PropTypes.oneOfType([ PropTypes.string, PropTypes.number ])
}

ProductQuantity.defaultProps = {
  quantity: 1,
  title: ''
}

export default ProductQuantity
