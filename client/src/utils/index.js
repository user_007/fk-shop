export const getMenuItems = (brands, categories) => {

  const brandsList = brands.map( el => ({
    key: el._id,
    name: `Товары ${el.name}`,
    linkState: { brands: [el.name] }
  }) ) || []

  const categoriesList = categories.map( el => ({
    key: el._id,
    name: el.name,
    linkState: { categories: [el.name] }
  }) ) || []

  // state: { brands: ['Rockwool'] }
  return [ ...brandsList, ...categoriesList ]

}


// export const getRoutes = () => {

//   return [
//     { path: "/products", name: "Товары", Component: ProductCatalog },
//     { path: "/products/:productId", name: "СТраница товара", Component: ProductPage },
//     {
//       path: "/cart/",
//       name: "Корзина оваров",
//       Component: CartPage
//     }
//   ];

// }
