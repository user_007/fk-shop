import { DATA_LOADING, FETCH_INITIAL_DATA, FETCH_PAGE_DATA, DATA_FAIL } from '../actions'

const initialState = {

  page: '',
  content: {},
  categories: [],
  brands: [],
  loading: false,
  errors: [],
  initialDataFetched: false

}

export default (state = initialState, { type, payload }) => {
  switch (type) {

    case FETCH_INITIAL_DATA:
      return { 
        ...state, 
        ...payload,
        loading: false,
        initialDataFetched: true 
      }

    case FETCH_PAGE_DATA:
      return { 
        ...state, 
        content: payload, 
        loading: false 
      }

    case DATA_LOADING:
      return { 
        ...state, 
        loading: true, 
        errors: false 
      };

    case DATA_FAIL:
      return {        
        ...state, 
        loading: false,
        errors: payload 
      }

    default:
      return { ...state, loading: false }

  }
}