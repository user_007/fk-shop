import { GET_NOTIFICATION, CLOSE_NOTIFICATION } from '../actions'


const initialState = {
  text: '',
  title: 'Оповещение',
  type: 'info',
  show: false
}

export default (state = initialState, { type, payload }) => {
  switch (type) {

    case GET_NOTIFICATION:
      return { ...state, ...payload, show: true }

    case CLOSE_NOTIFICATION:
      return {
        ...state,
        text: '',
        show: false
      }

    default:
      return { ...state }

  }
}
