import { FETCH_ORDER_DATA, ADD_PRODUCT_TO_CART, SUBMIT_ORDER_DATA, UPDATE_PRODUCT_IN_CART } from '../actions'


const initialState = {

  order: null,
  contacts: null

}

export default (state = initialState, { type, payload }) => {

  switch (type) {

    case FETCH_ORDER_DATA:
      return { 
        order: payload 
      }

    case ADD_PRODUCT_TO_CART:
      return { ...state, order: payload }

    case UPDATE_PRODUCT_IN_CART:
      return { ...state, order: payload }

    case SUBMIT_ORDER_DATA:
      return {
        ...state,
        order: null
      }

    default:
      return { ...state }

  }
}