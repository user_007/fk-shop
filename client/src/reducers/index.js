import { combineReducers } from 'redux';

import dataReducer from './dataReducer'
import cartReducer from './cartReducer'
import notificationReducer from './notificationReducer'

export default combineReducers({
  page: dataReducer,
  cart: cartReducer,
  notification: notificationReducer
})