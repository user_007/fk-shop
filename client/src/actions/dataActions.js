import { FETCH_PAGE_DATA, DATA_LOADING, FETCH_ORDER_DATA, DATA_FAIL, FETCH_INITIAL_DATA, SEND_CALLBACK, CLOSE_NOTIFICATION, GET_NOTIFICATION, FETCH_INITIAL_ADMIN_DATA } from '../actions'
import axios from 'axios'


// export const getStartupData = () => async dispatch => {

//   dispatch({ type: DATA_LOADING });

//   const _sid = localStorage.getItem('_sid');
//   let initialData;

//   if (!_sid) {

//     const request = await axios.post('/api/v1/sessions/', {});

//     const { data } = request.data;

//     localStorage.setItem('_sid', data._id)

//     initialData = data;

//   } else {

//     initialData = await axios.get(`/api/v1/sessions/${_sid}`);

//   }

//   console.log('INI DATA: ', initialData);


//   if (initialData.currentOrder) {

//     dispatch({ type: FETCH_ORDER_DATA, payload: initialData.currentOrder })

//   }

// }


export const getStartupData = () => dispatch => {

  dispatch({ type: DATA_LOADING });

  const _sid = localStorage.getItem('_sid');

  // console.log('SID: ', _sid);

  const getCategoriesRequest = axios.get('/api/v1/categories')
  const getBrandsRequest = axios.get('/api/v1/brands')

  const sessionRequest = _sid && _sid !== 'undefined' ? axios.get(`/api/v1/sessions/${_sid}`) :  axios.post('/api/v1/sessions/', {})


  Promise.all([sessionRequest, getCategoriesRequest, getBrandsRequest])
    .then( response => {

      const { data: { data: session } } = response[0]
      const { data: { data: categories } } = response[1]
      const { data: { data: brands } } = response[2]

      localStorage.setItem('_sid', session._id);

      
      if (session.currentOrder) {

        dispatch({ type: FETCH_ORDER_DATA, payload: session.currentOrder })
    
      }

      dispatch({ type: FETCH_INITIAL_DATA, payload: { brands, categories } })

    })
    .catch( err => {

      console.error('INIT DATA ERROR: ', err)
      dispatch({ type: DATA_FAIL, payload: err.response.data })

    })

}



export const loadPageData = (history, state = {}) => async dispatch => {

  dispatch({ type: DATA_LOADING })

  let endpoint = '';

  const { pathname } = history.location

  const stateIsEmpty = !Object.keys(state).length;

  console.log('ACTION PATHNAME: ', pathname)

  switch (pathname) {

    // case '/products/:productSlug':
      // endpoint = `/api/v1/products/${page.slug.split('/')[page.slug.split('/').length - 1]}`;
      // break;

    case '/products':
      endpoint = `/api/v1/products`;
      break;

    // case '/products/':
    //   endpoint = `/api/v1/products`;
    //   break;

    case '/admin':
      endpoint = `/api/v1/admin`
      break;

    case '/cart':
      endpoint = `/api/v1/orders/`;
      break;

    case 'contact-page':
      endpoint = '';
      break;

    default:
      endpoint = '/api/v1/products/brand-sorted'

  }


  try {

    const url = new URL(endpoint, `http://localhost:5000`)

    if (!stateIsEmpty) {

      Object.keys(state).forEach(key => {

        url.searchParams.set(key, state[key])

      })

    }

    const response = await axios.get(`${url.pathname}${url.search}`);

    const { data } = response.data;

    console.log('ACTION DATA: ', data);

    dispatch({ type: FETCH_PAGE_DATA, payload: data })

  } catch (err) {

    console.error('Request error: ', err)
    dispatch({ type: DATA_FAIL, payload: err.response.data })

  }

}


export const sendCallBack = data => async dispatch => {

  const notification = {
    content: 'Заказ на обратный звонок',
    senderPhone: data.phone,
    senderName: data.name,
    category: 'callback'
  }

  await axios.post('/api/v1/notifications', { notification })

  dispatch({ type: SEND_CALLBACK })
  dispatch({ 
    type: GET_NOTIFICATION, payload: { text: `${data.name.capitalize()}, 
    Ваш заказ на обратный звонок успешно зарегистрирован.` } 
  })

}



export const showNotification = ({title = 'Оповещение', text, type = 'info'}) => dispatch => {

  const payload = {
    text: text, title: title, type: type
  }

  dispatch({ type: GET_NOTIFICATION, payload: payload })

}


export const closeNotification = () => dispatch => {

  dispatch({ type: CLOSE_NOTIFICATION })

}
