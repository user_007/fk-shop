import axios from "axios";
import {
  ADD_PRODUCT_TO_CART,
  DATA_LOADING,
  DATA_FAIL,
  SUBMIT_ORDER_DATA,
  UPDATE_PRODUCT_IN_CART,
  GET_NOTIFICATION,
} from "../actions";

// send array of objects { productId, quantity }
export const addProductToCart = (orderId, order) => async (dispatch) => {

  const endpoint = orderId
    ? `/api/v1/orders/${orderId}/add-product`
    : `/api/v1/sessions/${localStorage.getItem("_sid")}/add-order`;

  try {
    const response = await axios.post(endpoint, order);

    const { data } = response.data;

    // console.log("ADD PRODUCT RESPONSE DATA: ", data);

    dispatch({ type: ADD_PRODUCT_TO_CART, payload: data });
  } catch (err) {

    console.error('Request error: ', err)
    dispatch({ type: DATA_FAIL, payload: err.response.data });
  }
};

export const updateCurrentOrder = (orderId, order) => async dispatch => {
  // console.log("UPD DATA: ", order);

  dispatch({ type: DATA_LOADING })

  try {

    const response = await axios.post(`/api/v1/orders/${orderId}`, order);

    const { data } = response.data;

    dispatch({ type: UPDATE_PRODUCT_IN_CART, payload: data });

  } catch (err) {

    console.error('Request error: ', err);
    dispatch({ type: DATA_FAIL, payload: err });

  }

};

export const submitOrder = (orderId, orderData) => async (dispatch) => {
  dispatch({ type: DATA_LOADING });

  try {
    // console.log('SUBMIT ORDER', orderId);
    // console.log('ORDER DATA: ', orderData);

    const response = await axios.post(
      `/api/v1/orders/${orderId}/submit`,
      orderData
    );

    // await Promise.all([
    //   axios.post(`/api/v1/orders/${order._id}/submit`, order),
    //   axios.post(`/api/v1/sessions/${localStorage.getItem('_sid')}`, { currentOrder: null })
    // ])

    const { data } = response.data;

    dispatch({ type: SUBMIT_ORDER_DATA });
    dispatch({
      type: GET_NOTIFICATION,
      payload: {
        text:
          "Заказ успешно зарегистрирован, в ближайшее время наш менеджер свяжется с Вами.",
        type: "success",
      },
    });
  } catch (err) {

    console.error('Request error: ', err);
    dispatch({ type: DATA_FAIL, payload: err.response.data });
  }
};
